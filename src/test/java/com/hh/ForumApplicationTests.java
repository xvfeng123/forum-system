package com.hh;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class ForumApplicationTests {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void contextLoads() {
        // redisTemplate 操作不同的数据类型 api和我们的指令是大致一样的
        // redisTemplate.opsForValue().  针对字符串类型的操作  类似String    和   bitmap   (尽量不要用中文，可能会有乱码)
        // redisTemplate.opsForHash().  操作  Hash
        // redisTemplate.opsForZSet().  操作  zset
        // redisTemplate.opsForList().  操作  List
        // redisTemplate.opsForSet().  操作  set
        // redisTemplate.opsForSet().  操作  set
        // redisTemplate.opsForGeo().  操作  geospatial
        // redisTemplate.opsForHyperLogLog().  操作  hyperloglog
        // redisTemplate.opsForHyperLogLog().  操作  hyperloglog

        // 除了基本的操作，还可以直接通过redisTemplate 操作事务  和 key 的 crud等

        // 获取 redis 连接对象   一般很少去用
//        RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
//        connection.flushDb();
//        connection.flushAll();

        redisTemplate.opsForValue().set("name", "haohao");
        System.out.println(redisTemplate.opsForValue().get("name"));

    }

}
