package com.hh;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapLearnInPython {
    public static void main(String[] args) {
//        newMap2();
        parseEntry();
    }


    private static void parseEntry(){
        Map<String, String[]> map = new HashMap<>();

        map.put("eno", new String[]{"1001"});
        map.put("ename", new String[]{"浩浩"});
        map.put("hobby", new String[]{"3", "6", "9"});

        System.out.println(map);

        Map<String, String> data = new HashMap<>();

        map.forEach((key, val)->{  // 栏目大 表达式
            int len = val.length;
            if (len == 1){
                data.put(key, val[0]);
            }else {
                StringBuilder content = new StringBuilder(val[0]);
                for (int i = 1; i < len; i++) {
                    content.append(",").append(val[i]);
                }
                data.put(key, content.toString());
            }
        });

        // 将键值对  导出 形成键值对集合
//        Set<Map.Entry<String, String[]>> entrySet = map.entrySet();
//        for (Map.Entry<String, String[]> entry : entrySet){
//            String[] value = entry.getValue();
//
//            if (value.length == 1){
//                data.put(entry.getKey(), value[0]);
//            }else {
//                StringBuilder content = new StringBuilder(value[0]);
//                int len = value.length;
//                for (int i = 1; i < len; i++) {
//                    content.append(",").append(value[i]);
//                }
//                data.put(entry.getKey(), content.toString());
//            }
//        }
        System.out.println(data);
    }

    /*
        创建一个 map   然后向里面放入数据
    * */
    private static void newMap(){
        Map<String, String> map = new HashMap<>();

        map.put("pid", "1001");
        map.put("pname", "浩浩");
        map.put("sex", "男");
        map.put("nation", "汉");

        System.out.println(map);
    }

    /*
        在HashMap 被实例化的时候   就像其中 放入数据
    * */
    private static void newMap2(){
//        Map<String, String> map = new HashMap<>(){};
        /*
            new HashMap<>(){} 放在堆 (上)

            Map<String, String> map 放在栈  (下)

            正常顺序是先在堆中 生成  new HashMap<>(){}  然后再在栈中生成对其的引用 Map<String, String> map
            一般情况下是 通过 map.put(key, value) 来对其进行元素的添加   这个时候的添加元素是会参照 map 的定义Map<String, String> 来进行类型判断校验

            这里new HashMap<>()  通过 {} 在定义变量时就对其内容进行初始化  即在生成 new HashMap<> 时就通过语句块向其 put 初始元素(如下)
            那这个时候新put的元素  怎么校验是否 符合类型要求呢?
            这个时候就要在 new HashMap<>() 定义的时候指定类型  --> new HashMap<String, String>()
            之后就可以在{}中通过 语句块 直接进行 put 初始化  map
        * */
        Map<String, String> map = new HashMap<String, String>(){
            {
                put("pid", "1001");
                put("pname", "浩浩");
                put("sex", "男");
                put("nation", "汉");
            }
        };

        System.out.println(map);
    }
}
