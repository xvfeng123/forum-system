package com.hh;

public class Emp {

    private String eno = null;
    private String name = null;
    private String sex = null;
    private String nation = null;
    private String hobby = null;

    public Emp() {

    }

    public Emp(String eno, String name, String sex, String nation, String hobby) {
        this.eno = eno;
        this.name = name;
        this.sex = sex;
        this.nation = nation;
        this.hobby = hobby;
    }

    public Emp(String name, String sex) {
//        this.name = name;
//        this.sex = sex;
        this(null, name, sex, null, null);  /*调用其他构造方法*/
    }

    public Emp(String name, String sex, String nation) {
//        this.name = name;
//        this.sex = sex;
//        this.nation = nation;
        this(null, name, sex, nation, null );
    }
}
