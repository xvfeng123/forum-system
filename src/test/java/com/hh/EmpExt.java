package com.hh;

import java.util.HashMap;
import java.util.Map;

public class EmpExt {

    private String eno = null;
    private String name = null;
    private String sex = null;
    private String nation = null;
    private String hobby = null;

    private EmpExt(Map<String, String> map) {
        this.eno = map.get("eno");   // 在该处进行 基于业务的清洗
        this.name = map.get("name");
        System.out.println("此处进行数据清洗...");
        this.sex = map.get("sex");
        this.nation = map.get("nation");
    }

    public EmpExt(String eno, String name, String sex, String nation, String hobby) {
//        Map<String, String> map = new HashMap<>();
//
//        map.put("eno", eno);
//        map.put("name", name);
//        map.put("sex", sex);
//        map.put("nation", nation);

//        this(map);  /*调用其他构造方法必须是第一条语句*/
        this(new HashMap<String, String>(){
            {
                put("eno", eno);
                put("name", name);
                put("sex", sex);
                put("nation", nation);
                put("hobby", hobby); // 这时增加一个 属性 hobby  就不用改各个 构造方法了

            }
        });  /*调用其他构造方法必须是第一条语句*/
    }

    public void show(){
        System.out.println("eno:" + this.eno);
        System.out.println("name:" + this.name);
        System.out.println("sex:" + this.sex);
        System.out.println("nation:" + this.nation);
    }

    public EmpExt(String name, String eno){
        this(new HashMap<String, String>(){
            {
                put("name", name);
                put("eno", eno);
            }
        });
    }
}
