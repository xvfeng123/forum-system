package com.hh.component;

/*该接口用于定义一些  文件（比如说帖子图片、用户头像等）保存的静态常量*/
public interface FilePathBase {
    //接口中默认就是  共有静态常量（public static final）  所以这里的static final 可以省略
    static final String baseUrl = "E:\\IdeaSpringProjects\\forum_system\\src\\main\\webapp\\";    //所有文件的基路径
    static final String barAvatar = "static\\images\\barAvatar\\"; //基路径下的贴吧头像
    static final String postImage = "static\\images\\postImage\\"; //基路径下的帖子图片
    static final String carouselPostImage = "static\\images\\carouselPostImage\\"; //基路径下的滚动帖宣传图片
    static final String postReplyImage = "static\\images\\postReplyImage\\"; //基路径下的帖子评论图片
    static final String postReplyReplyImage = "static\\images\\postReplyReplyImage\\";  //基路径下的评论回复图片
    static final String userAvatar = "static\\images\\userAvatar\\"; //基路径下的用户用户头像文件夹
    static final String frontEndBaseUrl = "http://localhost:8080"; //前端地址


    //以下是Linux   因为Windows是反斜杠\，Linux是斜杠/  所以必须区分开来
//    static final String baseUrl = "/home/haohao/forum_file_data/";    //所有文件的基路径
//    static final String barAvatar = "static/images/barAvatar/"; //基路径下的贴吧头像
//    static final String postImage = "static/images/postImage/"; //基路径下的帖子图片
//    static final String carouselPostImage = "static/images/carouselPostImage/"; //基路径下的滚动帖宣传图片
//    static final String postReplyImage = "static/images/postReplyImage/"; //基路径下的帖子评论图片
//    static final String postReplyReplyImage = "static/images/postReplyReplyImage/";  //基路径下的评论回复图片
//    static final String userAvatar = "static/images/userAvatar/"; //基路径下的用户用户头像文件夹
//    static final String frontEndBaseUrl = "https://www.myhaohao.top"; //前端地址

}
