package com.hh.component;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        /*
        如果controller跳转至页面，postHandle是没问题的。

        如果@ResponseBody注释 或者返回 ResponseEntity，在postHandle拦截器中修改请求头，是无效的。

        因为方法在先于postHandle方法之前将响应提交给HandlerAdapter（调用handler和Interceptor方法者），所以之后的修改就无效了。
        * */
        response.setHeader("Access-Control-Allow-Credentials", "true");   /*要在处理前处理，不知道为什么  在处理后执行他不会解析*/
//        response.setContentType("text/html;charset=utf-8");   //没用

        //chrome 最近把 SameSite 默认属性设置成 Lax  对于跨域访问（我这里是前后端），post请求，默认不发送 cookie，
        //要发送我后端设置的cookie（JsessionId） ， 要把 cookie的 SameSite 设置成 none。
        // 你用了SameSite=None 这个属性就必须加 Secure，而用这个 Secure 要求你发送 https 请求，老子吐了
//        response.setHeader("Set-Cookie", "HttpOnly;Secure;SameSite=None");

        return true; //放行


    }

    //在请求处理方法执行之后执行
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        System.out.println("------------处理后------------");
//        response.setHeader("Access-Control-Allow-Credentials", "true");   /*在这里处理它不被解析*/
    }
}

