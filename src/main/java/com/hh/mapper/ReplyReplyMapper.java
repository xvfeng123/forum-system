package com.hh.mapper;

import com.hh.pojo.ReplyReply;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
@Mapper
public interface ReplyReplyMapper {
    /*获取当前帖子评论回复 列表 中 最大的编号*/
    /*由于是使用序列配合触发器实现自增  所以下一条要插入的帖子评论回复id就是 当前maxId + 1 */
    /*由于在表中没有记录时这个时候 max(id)  得到的是null  基本数据类型接收不了  所以这里用其包装类  Integer*/
    Integer getMaxId();

    /*插入一条 帖子评论回复记录*/
    int insertRR(Map<String, Object> map);

    /*根据 map  来查询对应的 评论回复 (包含该条评论回复所拥有的子回复)*/
    ReplyReply getRRByMap(Map<String, Object> map);
}
