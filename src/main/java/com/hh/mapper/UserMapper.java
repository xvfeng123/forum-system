package com.hh.mapper;

import com.hh.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
@Repository
public interface UserMapper {
    /*新增一个用户*/
    int insertUser(User user);

    /*根据用户名查询用户*/
    User getUserByUname(@Param("uname") String uname);

    /*根据用户名和密码查询 用户（是否存在）*/
    User getUser(Map<String, String> map);
//    User getUser(@Param("uname") String uname, @Param("upwd") String upwd);

    /*更新一个用户*/
    int updateUser(Map<String, Object> map);
}
