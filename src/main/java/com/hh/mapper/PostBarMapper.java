package com.hh.mapper;

import com.hh.pojo.Category;
import com.hh.pojo.PostBar;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PostBarMapper {
    /*获取已有的贴吧列表*/
    List<PostBar> getPostBarListByMap(Map<String, Object> map);

    /*根据id  获取贴吧的详细信息*/
    PostBar getDetailedBarById(@Param("id") int barId);

    /*根据 id 删除一个贴吧（删除数据库记录  但不删除文件）*/
    int delPostBarById(@Param("id") int id);

    /*获取贴吧主键（id）序列的下一个值（当前值++，再返回++后的值    在触发器中直接使用currval作为新贴吧主键）*/
    int getNextVal();

    /*新增一个贴吧*/
    int addPostBar(PostBar postBar);

    /*新增一项贴吧和贴吧类别的关联  一次插入只新增一项*/
    int addBarCategoryRelevancy(Map<String, Integer> map);

    /*根据id获取贴吧(只包含 id  头像  描述 以及名称， 即只获取贴吧表中的4个字段)*/
    PostBar getSimpleBarById(@Param("id") int id);

    /*根据map中的值 查找贴吧（只包含 id  头像  描述 以及名称， 即只获取贴吧表中的4个字段）*/
    /*map  中可能包含 uname  根据该用户加入的贴吧*/
    List<PostBar> getSimpleBarByMap(Map<String, Object> map);

    /*检查贴吧是否是  特殊贴吧（私密）  （贴吧名称为  我的私密）*/
    int checkBarSpecial(@Param("barId") int bar);

    /*将所有贴吧的 推荐字段都重置 （设置为0 -- 不推荐）*/
    int resetAllBarRec();

    /*将 数组内的贴吧id 对应的贴吧设置为 推荐（推荐字段设置为 1）*/
    /*在调用本方法前应判断传进来的数组是否为空（或长度 == 0）  在这里要保证传进来的数组中 有元素*/
    int setBarsRec(@Param("barIds") int[] barIds);
}
