package com.hh.mapper;

import java.util.Map;

public interface RelevancyMapper {
    /*根据map里的值查询关系 （是否存在） 由于对应关系不同，这里用 Object 作为 返回值类型*/
    Object getRvcByMap(Map<String, Object> map);

    /*根据 map 里的值 在数据库表中增加一个关系 返回受影响的行数*/
    int addRvc(Map<String, Object> map);

    /*根据map中的值  在数据库表中删除 对应的记录 返回受影响的行数*/
    int deleteRvc(Map<String, Object> map);
}
