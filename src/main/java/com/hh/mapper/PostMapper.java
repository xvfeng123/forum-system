package com.hh.mapper;

import com.hh.pojo.Post;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PostMapper {
    /*获取当前帖子 列表 中 最大的编号*/
    /*由于是使用序列配合触发器实现自增  所以下一条要插入的帖子id就是 当前maxId + 1 */
    /*由于在表中没有记录时这个时候 max(id)  得到的是null  基本数据类型接收不了  所以这里用其包装类  Integer*/
    Integer getMaxId();

    /*插入一条 帖子记录*/
    int insertPost(Map<String, Object> map);

    /*列表要返回的帖子信息包括：帖子id、    （id（uname）用于a链接跳转时的信息查询依据）
                             贴吧id、贴吧名称
                             贴吧所属分类名称、分类id
                             帖子标题、  总回复数、 点赞数、浏览数
                             帖子纯内容
                             帖子图片地址（如果有的话）
                             发布人昵称  发布人uname    发布时间*/
    /*根据  总回复数量  查询帖子列表*/
    List<Post> getListByMap(Map<String, Object> map);
    /*根据  发布时间   查询帖子列表*/
//    List<Post> getListByTime();

    /*根据 帖子id 查询帖子详细信息（包括 帖子发布人、帖子详情、评论、回复信息等）*/
    Post getDetailedPostById(@Param("id") int id);

    /*根据帖子id 获取帖子的简单信息， 包括帖子内容标题、发表人、贴吧信息*/
    Post getSimplePostById(@Param("postId") int postId);

    /*根据用户名获取用户发表的 帖子总数*/
    int getPostCountByUser(@Param("uname") String uname);

    /*根据 帖子id 删除帖子   xml文件中按照@Param("id") 中的参数名 取值*/
    int deletePostById(@Param("id") int postId);

    /*根据帖子 id  将其浏览数++*/
    int addPostViewNum(@Param("postId") int postId);

    /*检查帖子是否是  特殊帖子（私密） （贴吧名称为  我的私密）*/
    int checkPostSpecial(@Param("postId") int post);

    /*将帖子设为置顶*/
    int setTop(@Param("postId") int postId);
    /*将帖子取消置顶*/
    int cancelTop(@Param("postId") int postId);
}
