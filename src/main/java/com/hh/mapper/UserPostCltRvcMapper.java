package com.hh.mapper;

import com.hh.pojo.UserPostCltRvc;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
@Repository
public interface UserPostCltRvcMapper extends RelevancyMapper{
    /*在此处 应该是根据帖子id 和 用户名进行查询   */
    /*map 中应该包含键分别为 “uname”和 “postId”的两个键值对*/
    @Override
    UserPostCltRvc getRvcByMap(Map<String, Object> map);
}
