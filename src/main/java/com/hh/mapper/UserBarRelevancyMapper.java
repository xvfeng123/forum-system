package com.hh.mapper;

import com.hh.pojo.UserBarRelevancy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
@Repository
public interface UserBarRelevancyMapper extends RelevancyMapper{
//    /**
//     * 根据贴吧编号和用户名查询表中是否有对应关系
//     * @param id  贴吧id
//     * @param uname 用户名
//     * @return 对应的用户贴吧关联（如果有的话）
//     */
//    UserBarRelevancy getRvcByIdName(@Param("id") int id, @Param("uname") String uname);
//
//    int addRvc(Map<String, Object> map);
//
//    /*根据map中的值  删除用户贴吧关联表中 对应的记录 */
//    int deleteRvc(Map<String, Object> map);


    /*在此处 应该是根据贴吧id 和 用户名进行查询  获取根据 用户名和帖子id进行查询 */
    /*map 中应该包含键分别为 “uname”和 “barId”的两个值*/
    @Override
    UserBarRelevancy getRvcByMap(Map<String, Object> map);

    /*根据用户名  查询用户加入的贴吧数*/
    int getBarCountByUser(@Param("uname") String uname);
}
