package com.hh.mapper;

import com.hh.pojo.PostReply;
import com.hh.pojo.ReplyReply;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
@Mapper
public interface PostReplyMapper {
    /*获取当前帖子评论 列表 中 最大的编号*/
    /*由于是使用序列配合触发器实现自增  所以下一条要插入的帖子评论id就是 当前maxId + 1 */
    /*由于在表中没有记录时这个时候 max(id)  得到的是null  基本数据类型接收不了  所以这里用其包装类  Integer*/
    Integer getMaxId();

    /*插入一条 帖子评论记录*/
    int insertPostReply(Map<String, Object> map);

    /*根据 map  来查询对应的 评论(简单信息 - 评论、评论所属帖子基本信息以及评论人信息  不包括评论下的回复)*/
    PostReply getSpPrByMap(Map<String, Object> map);

    /*根据用户名获取用户 发表的评论数*/
    int getPostReplyCountByUser(@Param("uname") String uname);
}
