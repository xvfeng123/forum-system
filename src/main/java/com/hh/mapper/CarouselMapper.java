package com.hh.mapper;

import com.hh.pojo.CarouselPost;
import com.hh.pojo.Category;
import com.hh.pojo.PostBar;
import com.hh.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CarouselMapper {
    /*新增一个 滚动帖*/
    int addCarouselPost(CarouselPost carouselPost);

    /*获取滚动帖主键（id）序列的下一个值（当前值++，再返回++后的值    在触发器中直接使用currval作为新滚动帖主键）*/
    int getNextVal();

    /*获取已有的滚动帖列表 滚动帖信息包括 帖子、发表人等的一些详细信息*/
    List<CarouselPost> getCrsListByMap(Map<String, Object> map);

    /*根据id 查询 滚动帖的简单信息（帖子id、描述、图片和顺序  不包括帖子的详细信息）*/
    CarouselPost getSimpleCrsById(@Param("crsId") int id);

    /*更新一个滚动帖， 比如顺序之类的*/
    int updateCarousel(Map<String, Object> map);

    /*根据 滚动帖id 删除滚动帖记录   不使用@Param("id")注解 默认按照参数名读取*/
    int deleteCrsPostById(int crsId);


}
