package com.hh.mapper;

import com.hh.pojo.Category;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CategoryMapper {
    /*新增一个类别*/
    int addCategory(Category category);

    /*获取已有的分类列表*/
    List<Category> getCategoryList();

    /*根据map中的值查询 类别*/
    Category getCategory(Map<String, Object> map);

    /*根据id 删除一个类别*/
    int delCategoryById(@Param("id") int id);
}
