package com.hh.mapper;

import com.hh.pojo.CarouselPost;
import com.hh.pojo.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface MessageMapper {
    /*根据uname 获取评论消息列表*/
    List<Message> getCommentListByUser(String uname);

    /*根据uname 获取回复消息列表 -- 包括评论回复*/
    List<Message> getCommentReplyListByUser(String uname);
    /*根据uname 获取回复消息列表 -- 包括二次回复*/
    List<Message> getReplyReplyListByUser(String uname);

    /*根据uname 获取回复消息列表 -- 包括点赞 帖子 和点赞 评论*/
    List<Message> getSbcListByUser(String uname);

    /*根据uname 获取回复消息列表 -- 包括收藏消息*/
    List<Message> getCltListByUser(String uname);

    /*发送一条消息 map为参数*/
    int sendMsg(Map<String, Object> map);

    /**
     * 将某个用户的某种消息类型的消息（比如评论消息） 都设置为已读
     * @param msgType 消息类型
     * @param uname 要设置的消息所属用户（接收人）
     * @return 受影响的行数
     */
    int setMsgsReaden(@Param("msgType") int msgType, @Param("toUname") String uname);

    /**
     * 清空用户的某种类型的所有消息
     * @param msgType 要清空的消息类型
     * @param uname 消息所属用户（接受人）
     * @return 受影响行数
     */
    int truncateUserMsgByType(@Param("msgType") int msgType, @Param("toUname") String uname);

    // 根据消息类型来获取用户的消息未读数, map中 应当包含 uname 和 msgType 两个参数
    int getUserUnreadenMsgNumByType(Map<String, Object> map);
}
