package com.hh.mapper;

import com.hh.pojo.UserPRSbcRvc;
import com.hh.pojo.UserPostSbcRvc;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
@Repository
public interface UserPRSbcRvcMapper extends RelevancyMapper{
    /*在此处 应该是根据帖子评论id 和 用户名进行查询   */
    /*map 中应该包含键分别为 “uname”和 “rrId”的两个键值对*/
    @Override
    UserPRSbcRvc getRvcByMap(Map<String, Object> map);
}
