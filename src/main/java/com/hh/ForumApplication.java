package com.hh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
//@EnableTransactionManagement //如果mybatis中service实现类中加入事务注解，需要此处添加该注解     没加这个注解好像事务也能成功配置
public class ForumApplication{

    public static void main(String[] args) {
        SpringApplication.run(ForumApplication.class, args);
        System.out.println("123");
    }

}
