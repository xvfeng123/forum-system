package com.hh.utils;

import com.hh.pojo.MessageModel;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class FileTool {

    /*根据所给的路径删除一个文件*/
    public static int deleteFile(String path)
    {
        File file = new File(path);
        if (file.exists()){  //文件存在的话
            return file.delete() ? 1 : 0;  //删除成功返回1  失败返回0
        }else {
            System.out.println("该文件不存在!");
            System.out.println("该文件路径：" + path);
            return 0;
        }
    }

    /*删除一个文件 数组（字符串类型 代表文件地址）*/
    // 成功删除 返回1  失败返回0
    public static int deleteFiles(String[] files)
    {
        int total = files.length;
        int deleted = 0; // 已删除的文件数

        for (String file : files){
            // 如果删除某文件失败  终止删除
            if (deleteFile(file)  == 0){
                System.out.println("文件删除终止！总文件数：" + total + ", 已删除文件数：" + deleted);
                return 0;
            }
            deleted++;
        }

        return 1;
    }


    /**
     * 将文件（数组）以相应的命名前缀保存至指定的目录下   单个文件命名格式：命名前缀-文件序号.文件类型
     * @param images  要保存的文件数组    MultipartFile 类型
     * @param saveNamePrefix  单个文件命名前缀
     * @param filePath 文件保存的路径  注意是 File  该类型可通过 String来初始话  new File(String)
     * @return 保存结果 MessageModel
     *                  200  有文件传入  且正常保存  对应的 msg  即 将所有保存后的文件通过空格连接组成的字符串
     *                  408  传入的文件数组为 null（没有文件传入）
     * @throws IOException   transferTo 写入文件时可能发生的异常  直接向上抛出即可
     */
    public static MessageModel saveImages(MultipartFile[] images, String saveNamePrefix, File filePath) throws IOException {
        MessageModel rs = new MessageModel(200, "文件写入结果初始化。。。");
        int i;  /*文件名 小数点位置*/
        String fileType;  /*文件类型 包括.*/
        String saveName;  /*文件保存名*/
        String separator = File.separator; //定义分隔符
        StringBuilder savedImages = new StringBuilder("");    /*StringBuilder 非线程安全  比线程安全 StringBuffer 更快*/

        /*(如果存在的话  遍历 images 图片数组  将其保存)*/
        if (images != null){
            for (int j = 0; j < images.length; j++) {
                i = Objects.requireNonNull(images[j].getOriginalFilename()).lastIndexOf('.');
                fileType = images[j].getOriginalFilename().substring(i);  /*包括.*/
                fileType = fileType.toLowerCase(); //一律转小写
                saveName = saveNamePrefix + "-" + (j+1) + fileType;

                images[j].transferTo(new File(filePath + separator + saveName));
                savedImages.append(saveName).append(" ");// 以空格分割
            }
            savedImages.deleteCharAt(savedImages.length() - 1);//删除最后一个 空格
            rs.setMsg(savedImages.toString()); //将转化后的结果 作为msg返回
        }else {
            rs.setResult_code(408);
            rs.setMsg("接收到的文件数组为空！");
        }

        return rs;
    }
}
