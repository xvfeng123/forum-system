package com.hh.utils;

import com.hh.pojo.MessageModel;
import com.hh.pojo.Post;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*此类是将service层一些重复的代码  抽象出来  的一个工具类 */
/*如getxxxList  他们都是返回一个map（如果获取成功再往map中添加查询到的list）*/
public class ServiceTool {

    /**
     * 函数功能：将列表存入map，并成功查询信息MessageModel存入map
     *
     * @param list 从数据库获取某种对象列表 如 CategoryList PostBarList UserList等
     * @return 结果map
     */
    public static Map<String, Object> getList(List list){
        Map<String, Object> map = new HashMap<>();
        MessageModel messageModel = new MessageModel(200, "查询成功！");

        map.put("list", list);
        map.put("rs", messageModel);
        return map;
    }

    /**
     * 函数功能： 将一个数据库查询出现异常的 错误信息MessageModel存入 map
     * @return  返回结果map
     */
    public static Map<String, Object> getListWrong(){
        Map<String, Object> map = new HashMap<>();
        MessageModel messageModel = new MessageModel(303, "数据库查询发生异常！");

        map.put("rs", messageModel);
        return map;
    }

    /**
     * 通过Java代码对结果list 做 分页
     * @param index 所需要的页码区间
     * @param pageSize 每页的大小
     * @return 分页后的区间对象
     */
    public static List listPage(List originList, int index, int pageSize){
        List list = new ArrayList<>();
        for (int i = (index-1)*pageSize; i < index*pageSize && i < originList.size(); i++){
            list.add(originList.get(i));
        }

        return list;
    }


}
