package com.hh.pojo;

import javafx.geometry.Pos;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/*消息类 包括 评论、回复、点赞和 收藏 四种消息通知*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    private int id;
    private String toUname;
    private Date informTime; // 消息时间
    private byte readen;
    private byte msgType;  // 1评论 2回复 3点赞 4收藏
//    private String fromUname;
//    private int postId;
//    private int prId;
//    private int rrId;
//    private int innerRrId;

    // 消息对应的帖子
    private Post post;
    // 消息发送人
    private User fromUser;
    // 评论
    private PostReply postReply;
    // 回复   (回复评论的内容   或者被二次回复的原回复)
    private ReplyReply replyReply;
    // 二次回复
    private ReplyReply innerRr;
}
