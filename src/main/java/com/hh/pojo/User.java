package com.hh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/* 用户类 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String uname;
    private String upwd;
    private String nickName;
    private String headImg;
    private String motto;  /*个性签名*/
    private String email;
    private String birthday;
    private Date regTime;  // 注册时间  年月日 时分
    //    private String area_province;  /*所在地区（省）*/
//    private String area_city; /*所在地区（市）*/
    private String[] area;
    private String areaProvince;
    private String areaCity;
    private String intro;
    private String role;

    /*通过areaProvince 和 areaCity来初始化 area */
    public void initArea(){
        this.setArea(new String[]{this.getAreaProvince(), this.getAreaCity()});
    }
}
