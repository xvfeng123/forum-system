package com.hh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/*贴吧类*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostBar {
    private int id;  /*贴吧编号  用户不可编辑（通过数据库触发器自动控制增长）*/
    private String name;
    private String barImg;  /*贴吧头像图片名称*/
    private String describe;
    private Date esTime;
    private byte setRecommend; // 是否为推荐贴吧   1是 0不是

    /*以下是非直接存储在贴吧表中的数据字段*/
    private List<Category> categoryList;  /*所属类别*/
    private int peopleNum; /*加入人数*/
    private int postNum;  /*帖子数*/
}
