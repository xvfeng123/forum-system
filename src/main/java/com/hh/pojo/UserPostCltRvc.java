package com.hh.pojo;

/*用户帖子 收藏关系类*/
public class UserPostCltRvc extends UserPostBaseRelevancy  {
    public UserPostCltRvc(String uname, int postId) {
        super(uname, postId);
    }

    public UserPostCltRvc() {
    }

    @Override
    public String toString() {
        return "UserPostCltRvc{}";
    }
}
