package com.hh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostReply {
    private int id;  //回复id
    private String content;  // 回复内容
    private String images; // 回复图片文件名  最多三张  每张以空格分隔
    private Date pbTime; // 发表回复时间

    private Post post;  // 对应的帖子
    private User user; // 回复人

    // 以下非数据库字段
    private int totalSubscribe; // 点赞数

    private List<ReplyReply> replyReplies; // 帖子评论 的 回复列表
}
