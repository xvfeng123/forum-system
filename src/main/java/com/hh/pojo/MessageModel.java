package com.hh.pojo;

public class MessageModel {
    /*
    200: 正常
    300: 数据库插入失败（返回值即受影响的行数 <1 ）
    301：数据库删除失败 （返回值即受影响的行数 <1 ）
    302：数据库更新失败 （返回值即受影响的行数 <1 ）
    303：数据库操作发生异常
    304：数据库查询结果为 空
    305：用户已存在
    306：（贴吧）类别（id）已存在
    307：用户不存在
    309： 帖子不存在或已被删除！
    322： 滚动帖顺序无变化
    310: 用户名或密码错误  （根据用户名和密码未查询到用户）
    408：文件写入失败
    409: 文件删除失败
    501: 获取session失败（设置参数为false）
    601： 用户已加入贴吧（单个）
    602： 用户未加入贴吧（单个）
    605： 用户未登录
    607： 用户加入贴吧个数为0（未加入任何贴吧）
    609： 当前用户无权限（权限不是 admin 管理员、没有权限查看对应的私密帖子）
    710： 私密帖 不可收藏
    714:  特殊（私密）贴吧不可退出
    800:  消息发送失败
    900:  未知错误
    * */
    private int result_code;  /*结果码*/
    private String msg;  /*结果信息*/

    public MessageModel() {
    }
    public MessageModel(int result_code, String msg) {
        this.result_code = result_code;
        this.msg = msg;
    }
    public MessageModel(int result_code) {
        this(result_code, "default msg");
    }
    @Override
    public String toString() {
        return "MessageModel{" +
                "result_code=" + result_code +
                ", msg='" + msg + '\'' +
                '}';
    }
    public int getResult_code() {
        return result_code;
    }
    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
}
