package com.hh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReplyReply {
    /*回复的回复
        包括：
            针对  帖子评论    的直接回复
            针对  帖子评论的回复  的回复
     * */
    private int id; // 回复编号
    private int type; // 指定该记录的回复对象是  帖子的评论（1）  还是  帖子评论回复  的回复（0）
    private String content; // 回复内容
    private String images; // 回复附带图片 最多3张  存储图片名称   空格分隔
    private Date pbTime; // 回复时间

    private PostReply toPostReply; // 要回复的 帖子评论 （如果该回复是针对此对象的话）
    private ReplyReply toReplyReply; //  要回复的 帖子评论的回复 （如果该回复是针对此对象的话）
    private User user; // 发表回复人
    private Post post; // 回复所属 帖子

    //以下是非数据库表字段
    private List<ReplyReply> replyReplies; // 帖子评论回复 的 回复列表
}
