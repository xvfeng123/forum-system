package com.hh.pojo;

/*和用户有关的关系  基类*/
public class UserBaseRelevancy {
    private String uname;

    @Override
    public String toString() {
        return "UserBaseRelevancy{" +
                "uname='" + uname + '\'' +
                '}';
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public UserBaseRelevancy() {
    }

    public UserBaseRelevancy(String uname) {
        this.uname = uname;
    }
}
