package com.hh.pojo;

/*用户  和 帖子  之间关系的基类 继承UserBaseRelevancy 用户关系基类*/
public class UserPostBaseRelevancy extends UserBaseRelevancy {
    private int postId;

    public UserPostBaseRelevancy(int postId) {
        this.postId = postId;
    }

    public UserPostBaseRelevancy(String uname, int postId) {
        super(uname);
        this.postId = postId;
    }

    public UserPostBaseRelevancy(String uname) {
        super(uname);
    }

    public UserPostBaseRelevancy() {
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    @Override
    public String toString() {
        return "UserPostBaseRelevancy{" +
                "postId=" + postId +
                '}';
    }
}
