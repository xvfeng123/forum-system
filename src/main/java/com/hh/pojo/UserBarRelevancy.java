package com.hh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.omg.CORBA.PRIVATE_MEMBER;

/*用户贴吧关联类*/
public class UserBarRelevancy extends UserBaseRelevancy{
//    private User user;
//    private PostBar postBar;

    private int barId; // 贴吧编号（主键id）
}
