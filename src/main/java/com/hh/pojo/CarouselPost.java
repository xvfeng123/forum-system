package com.hh.pojo;

import javafx.geometry.Pos;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/*首页滚动帖设置*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarouselPost {
    /*以下是数据库表字段*/
    private int id;  // 流水编号  序列自增     （基本数据类型默认初始化为 0）
    private String describe; // 描述文字       （对象默认 初始化为 null）
    private String images; //图片文件  这里有且仅有一张
    private int postId;  // 设置链接的帖子编号
    private int showOrder;  // 显示排列的顺序

    private Post post; // 设置链接的帖子

    public CarouselPost(CarouselPost carouselPost){
        this.id = carouselPost.id;
        this.describe = carouselPost.describe;
        this.images = carouselPost.images;
        this.postId = carouselPost.postId;
        this.showOrder = carouselPost.showOrder;
    }

}
