package com.hh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class UserPRSbcRvc extends UserBaseRelevancy {
    private int prId; /*帖子评论id*/


    public UserPRSbcRvc(String uname, int prId) {
        super(uname);
        this.prId = prId;
    }

    public UserPRSbcRvc() {
    }

    @Override
    public String toString() {
        return "UserPRSbcRvc{" +
                "prId=" + prId +
                '}';
    }

    public int getPrId() {
        return prId;
    }

    public void setPrId(int prId) {
        this.prId = prId;
    }
}
