package com.hh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Test;

/*贴吧类别（分类）*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    private int id;  //类别编号    它自己初始化  默认就为 0
    private String name;  //类别名称
    private String describe; //类别描述

    /*构造方法重载*/
    public Category(int id){
        this.id = id;
    }
}
