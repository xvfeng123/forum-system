package com.hh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    /*以下是数据库表字段*/
    private int id;  // 帖子编号  序列自增
    private Date pbTime;  //发表时间  年月日 时分
    private String title; //帖子标题
    private String images; //图片文件  最多5张 每张用空格分割
    private int viewNum; //浏览数
    private String content; //内容
    private String pure_content;  //帖子纯内容(不包括html标签)

    private PostBar postBar;  // 对应贴吧
    private User user;  // 发表人

    /*以下是帖子逻辑属性*/
    private int replyNum; //回复数
    private int totalReplyNum; //总回复数  （包括回复的回复）
    private int sbcNum; //点赞数
    private int collectNum; //收藏数

//    private boolean isTop;  // 是否贴吧置顶
    private Date setTopTime;  // 贴吧置顶 时间

    private List<PostReply> replies;  // 帖子评论
}
