package com.hh.service;

import com.hh.pojo.MessageModel;
import com.hh.pojo.PostBar;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface PostBarService {
    /*获取 已有的贴吧列表*/
    Map<String, Object> getPostBarList();

    /*根据 map 条件获取 贴吧列表*/
    Map<String, Object> getPostBarListByMap(Map<String, Object> map);

    /*根据贴吧id  获取 贴吧 的详细信息  包括当前登录用户 和 该贴吧的加入情况、  贴吧所属分类信息等等*/
    Map<String, Object> getDetailedBarInfoById(int barId, String uname);

    /*根据id删除一个贴吧   （包括数据库记录和文件） */
    MessageModel deletePostBarById(int id, String path);

    /*添加一个贴吧*/
    MessageModel addPostBar(String name, int[] checkedCategories, @RequestParam("file") MultipartFile headImg, String describe, String path);

    /**
     * 查询uname对应的user和barId对应的贴吧是否有对应的加入关系
     *  已加入返回 601
     *     * 未加入返回 602
     * */
    MessageModel checkUserBarRelevance(String uname, int barId);

    /**
     * 获取当前登录用户（session中的user）和各贴吧的加入关系
     * @param uname  当前登录用户用户名  若未登录，会被初始化为空字符串  ""
     * @param barList  贴吧列表
     * @return 当前用户和各贴吧对应的加入关系map  键为贴吧编号id  值为是否已加入   false表示未加入  true表示已加入
     *
     */
    Map<Integer, Boolean> getUserBarRvc(String uname, List<PostBar> barList);

    /*uname对应用户加入  barId对应的贴吧*/
    MessageModel userJoinBar(String uname, int barId);

    /*uname对应用户退出  barId对应的贴吧*/
    MessageModel userExitBar(String uname, int barId);

    /*设置推荐贴吧 （将参数数组内的贴吧设置为推荐 -- 1  其他设置为 不推荐 -- 0）*/
    MessageModel setRecommendBars(int[] barIds);
}
