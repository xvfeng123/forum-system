package com.hh.service;

import com.hh.pojo.MessageModel;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface MessageService {
    /**
     * 新增一条 帖子评论 消息（当前登录用户（session）发送，所以不用特意接收 发送者uname  直接从session 中拿即可）
     * @param postId 评论哪条
     * @param toUname 消息接收人  uname
     * @param prId 评论id
     * @param request 用于获取消息发送者 uname
     * @return 新增结果 MessageModel rs
     */
    MessageModel addPostReplyMsg(int postId, String toUname, int prId, HttpServletRequest request);


    /**
     * 发送回复消息
     * @param fromUname 消息发送者
     * @param postId 帖子id
     * @param toUname 消息接收人uname
     * @param prId 评论id, 如果 此条回复消息是  回复 给评论， 那该参数应为有效值； 否则为0 表示无效
     * @param rrId 回复id，如果 此条回复消息是  回复 给评论， 那该参数对应为回复的id；
     *                      如果 此条回复消息是  回复 给回复， 那该参数对应为被回复（原回复）的id；
     * @param inrrId 二次回复id， 如果 此条回复消息是  回复 给回复， 那该参数应为有效值；否则为0 表示无效
     * @return 新增结果 MessageModel rs
     */
    MessageModel addReplyReplyMsg(String fromUname, int postId, String toUname, int prId, int rrId, int inrrId);


    /**
     * 发送点赞消息 （可能是点赞 帖子或者 评论）
     * @param fromUname 消息发送人uname
     * @param toUname 消息接收人 uname
     * @param postId （如果点赞帖子）点赞的帖子id； （如果点赞评论）评论所属帖子id
     * @param prId  （如果点赞帖子）应该接收到0； （如果点赞评论）评论id
     * @return 新增结果 MessageModel rs
     */
    MessageModel addSubscribeMsg(String fromUname, String toUname, int postId, int prId);

    /**
     * 发送收藏消息 （收藏帖子）
     * @param fromUname 消息发送人
     * @param toUname 消息接收人（收藏的帖子的发表人）
     * @param postId 收藏的帖子id
     * @return 新增结果 MessageModel rs
     */
    MessageModel addCollectMsg(String fromUname, String toUname, int postId);


    /**
     * 根据用户名（消息接收人） 和 消息类型 来获取用户的消息列表（包括已读和未读，所有消息一次性读取）
     * @param msgType 消息类型
     * @param uname 消息接收人 uname
     * @return 获取结果rs messageModel 和 结果消息列表
     */
    Map<String, Object> getUserMsgsByType(int msgType, String uname);


    /**
     * 获取用户 各种类型的消息 未读数量
     * @param uname 消息接收人 uname
     * @return 获取结果rs messageModel 和 四种消息的未读数
     */
    Map<String, Object> getUserMsgUnreadNums(String uname);

    /**
     * 将用户的对应类型的消息都设置为 已读。
     * 在调用当前方法时，在前端已经判断用户该类型下是有未读消息的，所以如果 数据库受影响行数 < 1 表示异常
     * @param uname  要操作哪个用户的消息
     * @param msgType 消息类型
     * @return 设置结果
     */
    MessageModel setUserMsgsReadByType(String uname, int msgType);

    /**
     * 清空用户的某个类型的所有消息
     * @param uname 消息所属用户
     * @param msgType 消息类型
     * @return 清空结果，如果受影响行数（delete操作） < 1 也要额外作为一种消息提示
     */
    MessageModel truncateUserMsgByType(String uname, int msgType);

}
