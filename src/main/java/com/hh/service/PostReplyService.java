package com.hh.service;

import com.hh.pojo.MessageModel;
import com.hh.pojo.PostBar;
import com.hh.pojo.PostReply;
import com.hh.pojo.User;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface PostReplyService {
    /*发表一条帖子评论回复*/
    MessageModel publishPostReply(User user, MultipartFile[] images, String content, int postId, String path, HttpServletRequest request);

    /*根据用户名 和 帖子评论id 查询用户 和帖子评论的点赞关系  返回true表示已点赞   false表示未点赞*/
    Boolean getUserPrClt(String uname, int prId);

    /**
     * 获取当前登录用户（session中的user）和(某篇帖子下的)评论列表的点赞关系
     * @param uname  当前登录用户用户名  若未登录，会被初始化为空字符串  ""
     * @param prList  (某篇帖子下的)评论列表
     * @return 当前用户和各贴吧对应的加入关系map  键为帖子评论编号id  值为是否已加入   false表示未点赞  true表示已点赞
     *
     */
    Map<Integer, Boolean> getUserPRSbcRvc(String uname, List<PostReply> prList);

    /*根据用户名和帖子评论id  点赞帖子评论*/
    MessageModel subscribePr(String uname, int prId);
    /*根据用户名和帖子评论id  取消点赞帖子评论*/
    MessageModel cancelSbcPr(String uname, int prId);
}
