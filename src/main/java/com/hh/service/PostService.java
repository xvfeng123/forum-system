package com.hh.service;

import com.hh.pojo.MessageModel;
import com.hh.pojo.Post;
import com.hh.pojo.User;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface PostService {

    /*根据用户名查询用户加入的贴吧信息*/
    /*返回值应该包括查询结果  messageBox  以及用户加入的贴吧列表list（如果查询正常的话）*/
    Map<String, Object> getUserBarList(User user);

    /*发表一篇帖子*/
    MessageModel publishPost(User user, MultipartFile[] images, int barId, String title, String content, String pure_content, String path);

    /*根据 帖子id 删除 帖子（由于外键的 on delete cascade 帖子评论、评论回复表中的对应记录也会被删除）*/
    /*uname 删帖人，用于辨别是谁在删帖，是否有权限。正常情况下都是帖子主人的uname*/
    MessageModel deletePostById(HttpServletRequest request, HttpServletResponse response, int postId) throws IOException, ServletException;

    /*/*根据  总回复数量  查询帖子列表*/
    Map<String, Object> getListByReply(int index, int pageSize);

    /*/*根据 关键字搜索 排序参数排序  查询帖子列表  分页返回结果*/
    Map<String, Object> getSearchedListByPage(Map<String, Object> map) throws ServletException, IOException;

    /*/*根据  帖子发表时间  查询帖子列表*/
    Map<String, Object> getListByTime(int index, int pageSize);

    /*/*根据  传入的排序规则（可以是 时间 或 总回复数  根据参数来判断）  查询帖子列表  相当于time 和 reply 结合*/
    Map<String, Object> getListBySortType(int index, int pageSize, String accordingTo);

    // 根据map 中的条件 做sql ，返回结果帖子列表的总记录数
    int getTotalNum(Map<String, Object> map);

    /*根据 帖子编号获取有关的 帖子详细信息（包括帖子基本信息、评论信息、回复信息等）*/
    Post getPostInfoById(int id);

    /**
     * 据用户名 和 帖子id 查询用户和该帖子的有关关系（包括帖子的点赞、收藏以及贴吧的加入关系）
     * @param uname 用户名（应该是session 中已登录的用户）
     * @param postId 帖子id
     * @return 用户名对应用户和帖子的各关系 返回一个map  键为String指明是什么关系，值为Boolean 指定是否
     */
    Map<String, Boolean> getUserPostRvc(String uname, int postId);

    /*true表示 已加入、已点赞、已收藏登。反之*/
    /*根据用户名 和 帖子id 查询用户 和帖子的点赞关系*/
    Boolean getUserPostSbc(String uname, int postId);
    /*根据用户名 和 帖子id 查询用户 和帖子的收藏关系*/
    Boolean getUserPostClt(String uname, int postId);
    /*根据用户名 和 帖子id 查询用户 和帖子所属贴吧的加入关系    */
    /*没有对用户名做 非空判断，如果传入的 uname 为 null，只要 barId存在，那也会返回true  。所以要注意先判断 uname 是否为 null*/
    Boolean getUserPostBarRvc(String uname, int postId);

    /*根据用户名和贴吧id  收藏帖子*/
    MessageModel collect(String uname, int postId);
    /*根据用户名和贴吧id  取消收藏帖子*/
    MessageModel cancelClt(String uname, int postId);
    /*根据用户名和贴吧id  点赞帖子*/
    MessageModel subscribe(String uname, int postId);
    /*根据用户名和贴吧id  取消点赞帖子*/
    MessageModel cancelSbc(String uname, int postId);
    /*设置帖子 贴吧置顶(operateType == 1)（或取消置顶 operateType == 0）*/
    MessageModel setTopPost(int postId, int operateType);

    /*点击帖子  将其浏览数++*/
    MessageModel viewPost(int postId);

    /*获取 贴吧置顶帖子*/
    Map<String, Object> getTopPostsByBar(int barId);

    /**
     * 检查用户是否有权限访问该帖子（只有自己可以访问自己的私密贴）
     * @param post 要检查的帖子
     * @param user 登录用户（访问用户、被检查的用户）
     * @return  true表示有权限可以查看，false表示没有权限、不可查看
     */
    boolean checkViewAuthority(Post post, User user);
}
