package com.hh.service;

import com.hh.pojo.MessageModel;
import com.hh.pojo.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface ReplyReplyService {
    /*发表一条帖子评论回复*/
    Map<String, Object> publishRR(User user, MultipartFile[] images, int toPostReply, int toReplyReply, String content, int postId, String path);
}
