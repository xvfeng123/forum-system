package com.hh.service.impl;

import com.hh.mapper.MessageMapper;
import com.hh.mapper.PostMapper;
import com.hh.mapper.PostReplyMapper;
import com.hh.mapper.UserPRSbcRvcMapper;
import com.hh.pojo.*;
import com.hh.service.MessageService;
import com.hh.service.PostReplyService;
import com.hh.utils.FileTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PostReplyServiceImpl implements PostReplyService {

    @Autowired
    private PostReplyMapper prMapper;

    @Autowired
    private UserPRSbcRvcMapper prSbcRvcMapper;

    @Autowired
    private PostMapper postMapper;

    @Resource
    private MessageService messageService;

    @Override
    public Boolean getUserPrClt(String uname, int prId) {
        HashMap<String, Object> map = new HashMap<>();

        map.put("prId", prId);
        map.put("uname", uname);

        return prSbcRvcMapper.getRvcByMap(map) != null;
    }

    @Override
    public Map<Integer, Boolean> getUserPRSbcRvc(String uname, List<PostReply> prList) {
        Map<Integer, Boolean> map = new HashMap<>();

        for (PostReply postReply : prList) {
            if (uname == null){  //用户名为空字符串表示 当前未登录
                map.put(postReply.getId(), false); //false表示未点赞  true表示已点赞  未登录默认显示为false
                continue;
            }
            map.put(postReply.getId(), getUserPrClt(uname, postReply.getId()));
        }

        return map;
    }

    /*
            返回:
            605     未登录
            300     数据库插入失败
            303     数据库插入发生异常
            408     文件写入发生异常
            200     正常插入
        * */
    @Override
    public MessageModel publishPostReply(User user, MultipartFile[] images, String content, int postId, String path, HttpServletRequest request) {
        MessageModel rs = new MessageModel(200, "回复成功!");
        Integer maxId;  // 已有的最后一条帖子评论的id\
        /*当前要插入的 帖子评论的  id  仅用作保存图片文件名使用， 实际评论的id编号通过触发器自动插入*/
        /* 另外，还用作 消息发送id绑定*/
        int prId;
        File filePath = new File(path);  //文件上传路径（不包括文件名）
        Map<String, Object> map = new HashMap<>();  //传入 mapper的  map (插入操作)
        MessageModel savedImagesRs;

        if (user == null){ //传来的user  为空   则未再session中取到
            rs.setResult_code(605);
            rs.setMsg("目前用户未登录!请先登录!");
        }else {
            /*访问数据库   得到当前 最后一篇帖子评论回复的id  下一篇帖子(即现在要插入的)id即是 max(id) + 1*/
            maxId = prMapper.getMaxId();
            if (maxId == null){  //目前 表中还没有记录  所以插入的是第一条  id为（1到7是测试数据，自增序列从8开始）
                prId = 8;
            }else {
                prId = maxId + 1;
            }

            try {
                /*(如果存在的话  遍历 images 图片数组  将其保存)*/
                savedImagesRs = FileTool.saveImages(images, "" + prId, filePath);
                if (savedImagesRs.getResult_code() == 200){ //正常写入的话，将 其值放入mapper的 insert  参数map
                    map.put("images", savedImagesRs.getMsg());
                }

                /*将帖子评论回复记录插入数据库表  参数为map*/
                map.put("userName", user.getUname());
                map.put("content", content);
                map.put("postId", postId);

                if (prMapper.insertPostReply(map) < 1){
                    rs.setResult_code(300);
                    rs.setMsg("数据库（帖子评论）插入失败！");
                }

                // 发表评论成功后 向帖子用户发送评论消息 (在方法里面有 try catch 包括)
                String toUname = postMapper.getSimplePostById(postId).getUser().getUname(); // 消息接收人
                if (!user.getUname().equals(toUname)){ // 评论人不是发帖人才发送消息
                    MessageModel msgSendRs = messageService.addPostReplyMsg(postId, toUname, prId, request);
                    if (msgSendRs.getResult_code() != 200){
                        msgSendRs.setResult_code(800); // 可能是数据库插入错误、异常等  这里统一设置为错误码 800，方便前端提示（而非报错）
                        return msgSendRs;
                    }
                }

            }catch (IOException e){
                rs.setResult_code(408);
                rs.setMsg("文件写入发生异常。");
                e.printStackTrace();
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库插入（帖子评论）发生异常。");
                e.printStackTrace();
            }
        }

        return rs;
    }


    @Override
    public MessageModel subscribePr(String uname, int prId) {
        MessageModel rs = new MessageModel(200, "点赞成功！");
        Map<String, Object> map;

        if (uname == null){
            rs.setResult_code(605);
            rs.setMsg("当前用户未登录！请登录后再点赞。");
        }else {
            try {
                map = new HashMap<>();
                map.put("uname", uname);
                map.put("prId", prId);

                if (prSbcRvcMapper.addRvc(map) < 1){
                    rs.setResult_code(300);
                    rs.setMsg("数据库插入失败！点赞失败！");
                    System.out.println("点赞失败！（受影响行数 < 1）");
                }else {
                    // 点赞操作 成功， 发送点赞消息给消息接收人（帖子发表人）
                    Map<String, Object> mapperMap = new HashMap<>();
                    mapperMap.put("prId", prId);
                    PostReply spPrByMap = prMapper.getSpPrByMap(mapperMap);

                    String toUname = spPrByMap.getUser().getUname(); // 消息接收人
                    int postId = spPrByMap.getPost().getId();
                    if (!uname.equals(toUname)){ // 点赞人不是发帖人才发送消息
                        MessageModel msgSendRs = messageService.addSubscribeMsg(uname, toUname, postId, prId);  // 即使点赞评论也要 传送有效帖子id
                        if (msgSendRs.getResult_code() != 200){
                            msgSendRs.setResult_code(800); // 可能是数据库插入错误、异常等  这里统一设置为错误码 800，方便前端提示（而非报错）
                            return msgSendRs;
                        }
                    }
                }
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库插入发生异常！");
                System.out.println("点赞发生异常！");
                e.printStackTrace();
            }
        }

        return rs;
    }

    @Override
    public MessageModel cancelSbcPr(String uname, int prId) {
        MessageModel rs = new MessageModel(200, "取消点赞成功！");
        Map<String, Object> map;

        if (uname == null){
            rs.setResult_code(605);
            rs.setMsg("当前用户未登录！请登录后再取消点赞。");
        }else {
            try {
                map = new HashMap<>();
                map.put("uname", uname);
                map.put("prId", prId);

                if (prSbcRvcMapper.deleteRvc(map) < 1){
                    rs.setResult_code(301);
                    rs.setMsg("数据库删除失败！取消点赞失败！");
                    System.out.println("取消点赞失败！（受影响行数 < 1）");
                }
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库删除发生异常！");
                System.out.println("取消点赞发生异常！");
                e.printStackTrace();
            }
        }

        return rs;
    }
}
