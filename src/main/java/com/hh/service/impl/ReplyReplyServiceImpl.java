package com.hh.service.impl;

import com.hh.mapper.PostReplyMapper;
import com.hh.mapper.ReplyReplyMapper;
import com.hh.pojo.MessageModel;
import com.hh.pojo.ReplyReply;
import com.hh.pojo.User;
import com.hh.service.MessageService;
import com.hh.service.ReplyReplyService;
import com.hh.utils.FileTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class ReplyReplyServiceImpl implements ReplyReplyService {

    @Autowired
    private ReplyReplyMapper rrMapper;

    @Autowired
    private PostReplyMapper postReplyMapper;

    @Resource
    private MessageService messageService;

    /*
        返回:
        605     未登录
        300     数据库插入失败
        303     数据库插入发生异常
        408     文件写入发生异常
        200     正常插入
    * */
    @Override
    public Map<String, Object> publishRR(User user, MultipartFile[] images, int toPostReply, int toReplyReply,
                                         String content, int postId, String path) {
        MessageModel rs = new MessageModel(200, "回复成功!");
        Integer maxId;  // 已有的最后一条帖子评论回复的id\
        int rrId;  /*当前要插入的 评论回复的  id  仅用作保存图片文件名使用， 实际评论回复的id编号通过触发器自动插入*/
        File filePath = new File(path);  //文件上传路径（不包括文件名）
        Map<String, Object> map = new HashMap<>();  //传入 mapper的  map (插入操作)
        Map<String, Object> queryMap = new HashMap<>();  //传入 mapper的  map (查询操作)
        int type;  /*该条回复的类型  针对评论（1）  针对评论回复（1）*/
        MessageModel savedImagesRs;
        HashMap<String, Object> rsMap = new HashMap<>(); //返回的结果map

        if (user == null){ //传来的user  为空   则未再session中取到
            rs.setResult_code(605);
            rs.setMsg("目前用户未登录!请先登录!");
        }else {
            /*访问数据库   得到当前 最后一篇帖子评论回复的id  下一篇帖子(即现在要插入的)id即是 max(id) + 1*/
            maxId = rrMapper.getMaxId();
            if (maxId == null){  //目前 表中还没有记录  所以插入的是第一条  id为（1、2是测试数据，自增序列从3开始）
                rrId = 3;
            }else {
                rrId = maxId + 1;
            }

            try {
                /*(如果存在的话  遍历 images 图片数组  将其保存)*/
                savedImagesRs = FileTool.saveImages(images, "" + rrId, filePath);
                if (savedImagesRs.getResult_code() == 200){ //正常写入的话，将 其值放入mapper的 insert  参数map
                    map.put("images", savedImagesRs.getMsg());
                }

                if (toReplyReply == 0){//   该条回复是针对 帖子评论本身的   否则是针对评论回复的
                    type = 1;  // toReplyReply 为null  不插入相应字段
                }else{
                    type = 0;
                    map.put("toReplyReply", toReplyReply);
                }
                /*将帖子评论回复记录插入数据库表  参数为map*/
                map.put("toPostReply", toPostReply);
                map.put("type", type);
                map.put("userName", user.getUname());
                map.put("content", content);
                map.put("postId", postId);

                if (rrMapper.insertRR(map) < 1){
                    rs.setResult_code(300);
                    rs.setMsg("数据库（帖子评论回复）插入失败！");
                }else{
                    // 回复 发布成功， 此时再 发送回复消息（如果回复的是评论，则发给评论人，如果是回复的也是回复，则发给原回复人）
                    String toUname = null;
                    int replyReplyId; // 传给 发送消息用的（回复）rrid 根据情况来设置值
                    int inrrId; // 传给 发送消息用的（二次回复）inrrId 根据情况来设置值
                    int toPrId; // 传给 发送消息用的（评论）prId 根据情况来设置值

                    Map<String, Object> mapperMap = new HashMap<>(); // 传给mapper层的参数 map
                    if (toReplyReply == 0){ // 回复给评论
                        mapperMap.put("prId", toPostReply);
                        toUname = postReplyMapper.getSpPrByMap(mapperMap).getUser().getUname();
                        inrrId = 0;
                        replyReplyId = rrId;
                        toPrId = toPostReply;
                    }else { // 回复给 回复
                        mapperMap.put("id", toReplyReply);
                        toUname = rrMapper.getRRByMap(mapperMap).getUser().getUname();
                        inrrId = rrId;
                        replyReplyId = toReplyReply;
                        toPrId = 0;
                    }

                    if (!user.getUname().equals(toUname)){ // 回复人不是被回复（评论）人才发送消息
                        MessageModel msgSendRs = messageService.addReplyReplyMsg(user.getUname(), postId, toUname, toPrId, replyReplyId, inrrId);
                        if (msgSendRs.getResult_code() != 200){
                            rs.setResult_code(800); // 可能是数据库插入错误、异常等  这里统一设置为错误码 800，方便前端提示（而非报错）
                            rs.setMsg(msgSendRs.getMsg());
                        }
                    }
                }
            }catch (IOException e){
                rs.setResult_code(408);
                rs.setMsg("文件写入发生异常。");
                e.printStackTrace();
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库插入（帖子评论回复）发生异常。");
                e.printStackTrace();
            }

            // 如果正常插入的话  返回刚插入的  rr
            if (rs.getResult_code() == 200){
                queryMap.put("id", rrId);
                try {
                    ReplyReply rr = rrMapper.getRRByMap(queryMap);
                    rsMap.put("rr", rr);
                }catch (Exception e){
                    rs.setResult_code(303);
                    rs.setMsg("数据库查询（帖子评论回复）发生异常。");
                    e.printStackTrace();
                }
            }
        }

        rsMap.put("rs", rs);
        return rsMap;
    }
}
