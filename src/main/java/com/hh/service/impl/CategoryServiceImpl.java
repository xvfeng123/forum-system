package com.hh.service.impl;

import com.hh.mapper.CategoryMapper;
import com.hh.pojo.Category;
import com.hh.pojo.MessageModel;
import com.hh.service.CategoryService;
import com.hh.utils.ServiceTool;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Test
    public void testInt(){
        Category category = new Category();
        System.out.println(category);
    }
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public MessageModel judgeIdUsed(int id) {
        MessageModel messageModel = new MessageModel(200, "编号可用。");
        HashMap<String, Object> map = new HashMap<>();

        map.put("id", id);
        try {
            if (categoryMapper.getCategory(map) != null){
                messageModel.setResult_code(306);
                messageModel.setMsg("抱歉，该类别编号已被使用过.");
            }
        }catch (Exception e){
            messageModel.setResult_code(303);
            messageModel.setMsg("数据库查询发生异常!");
            e.printStackTrace();
        }
        return messageModel;
    }

    @Override
    public MessageModel deleteCategoryById(int id) {
        MessageModel messageModel = new MessageModel(200, "分类删除成功！");

        try {
            if (categoryMapper.delCategoryById(id) < 1){
                messageModel.setResult_code(301);
                messageModel.setMsg("数据库删除失败。");
            }
        }catch (Exception e){
            messageModel.setResult_code(303);
            messageModel.setMsg("数据库删除发生异常!");
            e.printStackTrace();
        }
        return messageModel;
    }

    @Override
    public Map<String, Object> getCategoryList() {
//        Map<String, Object> map = new HashMap<>();
//        MessageModel messageModel = new MessageModel(200, "查询成功！");
//
//        try {
//            map.put("list", categoryMapper.getCategoryList());
//        }catch (Exception e){  /*数据库查询发生异常*/
//            messageModel.setResult_code(303);
//            messageModel.setMsg("数据库查询发生异常！");
//        }
//        map.put("rs", messageModel);
//        return map;
        try {
            return ServiceTool.getList(categoryMapper.getCategoryList());
        }catch (Exception e){  /*数据库查询发生异常*/
            e.printStackTrace();
            return ServiceTool.getListWrong();
        }
    }

    @Override
    public MessageModel addCategory(Category category) {
        MessageModel messageModel = new MessageModel(200, "添加成功！");

        try {
            if (categoryMapper.addCategory(category) < 1){
                messageModel.setResult_code(300);
                messageModel.setMsg("添加失败，（数据库插入失败。）");
            }
        }catch (Exception e){
            messageModel.setResult_code(303);
            messageModel.setMsg("添加失败，数据库插入发生异常。");
            e.printStackTrace();
        }
        return messageModel;
    }
}
