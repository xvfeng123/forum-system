package com.hh.service.impl;

import com.hh.component.FilePathBase;
import com.hh.controller.UserController;
import com.hh.mapper.*;
import com.hh.pojo.*;
import com.hh.service.MessageService;
import com.hh.service.PostService;
import com.hh.service.UserService;
import com.hh.utils.FileTool;
import com.hh.utils.ServiceTool;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostBarMapper postBarMapper;

    @Autowired
    private PostMapper postMapper;

    @Autowired
    private UserPostSbcRvcMapper userPostSbcRvcMapper;
    @Autowired
    private UserPostCltRvcMapper userPostCltRvcMapper;
    @Autowired
    private UserBarRelevancyMapper userBarRelevancyMapper;

    @Resource
    private MessageService messageService;
    @Resource
    private UserService userService;

    @Override
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000,rollbackFor=Exception.class)
    public MessageModel deletePostById(HttpServletRequest request, HttpServletResponse response, int postId) throws IOException, ServletException {
        // 由于外键的 on delete cascade删除帖子记录后， 帖子评论、评论回复表中的对应记录也会被删除
        MessageModel messageModel = new MessageModel(200, "帖子删除成功！");
        // 根据帖子id 获取 帖子对象
        Post post = postMapper.getDetailedPostById(postId); // 要删除的帖子
        String[] imgFiles;  // 要删除的图片文件
        String path; // 图片地址前缀路径
        ArrayList<String> failTypes = new ArrayList<>(); // 删除失败的文件类型
        int right;
        HashMap<String, String> params; /* 前端errorPage 地址栏添加的参数*/
        String errSection; // 错误模块（标题）
        String errDesc;    // （具体描述）
        String redctType; //（0显示登录按钮  1显示返回首页按钮）

        // 判断是否有权限删除帖子
        right = canDelPost(request, postId);
        if (right != 0){  // 0 表示有权限
            // 补充参数重定向到前端错误页面
            params = new HashMap<>();
            errSection = "帖子删除";

            if (right == 1){ // 未登录
                errDesc = "当前未登录，请先登录！";
                redctType = "0";
            }else{ //   right == 2 没有权限（当前登录用户不是发帖人）
                errDesc = "抱歉，您当前没有权限删除该贴！";
                redctType = "1";
            }

            params.put("errSection", URLEncoder.encode(errSection, "utf8"));
            params.put("errDesc", URLEncoder.encode(errDesc, "utf8"));
            params.put("redctType", URLEncoder.encode(redctType, "utf8"));
            request.setAttribute("params", params);
            request.getRequestDispatcher("/user/toErrorPage").forward(request, response);
            return null;
        }

        try {
            // 数据库帖子记录（对应的评论、回复记录也自动随之删除）
            if (postMapper.deletePostById(postId) < 1){ //删除记录受影响行数不为1
                messageModel.setResult_code(301);
                messageModel.setMsg("数据库删除失败。");
            }else {  //数据库记录删除正常  即受影响行数 = 1   再删除文件
                // 删除 帖子图片 帖子id-1.xxx 空格分隔
                if (!"noImg".equals(post.getImages())){
                    imgFiles = post.getImages().split(" ");
                    path = FilePathBase.baseUrl + FilePathBase.postImage;
//                    for (String file : imgFiles){
//                        file = path + file; // 按值传递  这里只是修改的部分，原数组没有改变
//                    }
                    for (int i = 0; i < imgFiles.length; i++) {
                        imgFiles[i] = path + imgFiles[i];
                    }
                    if (FileTool.deleteFiles(imgFiles) == 0){
                        failTypes.add("帖子图片");
                    }
                }

                // 删除 评论图片 评论id-1.xxx 空格分隔
                List<PostReply> replies = post.getReplies();
                for (int i = 0; i < replies.size(); i++) {
                    PostReply reply = replies.get(i);
                    if (!"noImg".equals(reply.getImages())){
                        imgFiles = reply.getImages().split(" ");
                        path = FilePathBase.baseUrl + FilePathBase.postReplyImage;
//                        for (String file : imgFiles){
//                            file = path + file;
//                        }
                        for (int j = 0; j < imgFiles.length; j++) {
                            imgFiles[j] = path + imgFiles[j];
                        }
                        if (FileTool.deleteFiles(imgFiles) == 0){
                            failTypes.add("评论" + i + " 图片");
                        }
                    }

                    // 删除 回复图片 回复id-1.xxx 空格分隔
                    List<ReplyReply> replyReplies = reply.getReplyReplies();
                    for (int j = 0; j < replyReplies.size(); j++) {
                        ReplyReply rr = replyReplies.get(j);
                        if (!"noImg".equals(rr.getImages())){
                            imgFiles = rr.getImages().split(" ");
                            path = FilePathBase.baseUrl + FilePathBase.postReplyReplyImage;
//                            for (String file : imgFiles){
//                                file = path + file;
//                            }
                            for (int m = 0; m < imgFiles.length; m++) {
                                imgFiles[m] = path + imgFiles[m];
                            }
                            if (FileTool.deleteFiles(imgFiles) == 0){
                                failTypes.add("评论" + i + " -> 回复" + j + " 图片");
                            }
                        }
                    }

                }

                // 设置 错误信息
                if (!failTypes.isEmpty()){
                    messageModel.setResult_code(409);
                    StringBuilder failMsg = new StringBuilder();
                    for (int i = 0; i < failTypes.size(); i++) {
                        failMsg.append(failTypes.get(i));
                        if (i < failTypes.size()-1){ // 如果不是最后一个 则加上、
                            failMsg.append("、");
                        }
                    }
                    failMsg.append("文件删除失败！");
                    messageModel.setMsg(failMsg.toString());
                }
            }
        }catch (Exception e){
            messageModel.setResult_code(303);
            messageModel.setMsg("数据库删除发生异常!");
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();  //由于手动catch了异常， 此处需要手动进行事务回滚
        }

        return messageModel;
    }

    /**
     * 判断当前登录用户是否有权限 删除该帖子
     * @param postId 要删除的帖子id
     * @return 0 表示有权限  1 表示未登录  2表示没有权限
     */
    private int canDelPost(HttpServletRequest request, int postId){
        int rs = 2;
        int right = userService.checkUserRight(request).getResult_code(); // 200 当前登录管理员，605 未登录， 609 普通用户

        if (right == 605){  // 未登录
            rs = 1;
        }else if (right == 200){  // 已作为管理员登录
            rs = 0;
        }else { // 已作为普通用户登录
            // 对比当前登录用户uname 是否为帖子对应主人的 uanme
            if (UserController.getCurrentUser(request).getUname().equals(postMapper.getSimplePostById(postId).getUser().getUname())){
                rs = 0;
            }
        }

        return rs;
    }

    @Override
    public boolean checkViewAuthority(Post post, User user) {
        // 判断是否是私密帖  如果不是私密，就直接返回true表示可以访问
        if (!"我的私密".equals(post.getPostBar().getName())){
            return true;
        }

        if (user == null){
            return false;
        }else {
            return post.getUser().getUname().equals(user.getUname());  // 当前登录用户和帖子发表人用户uname一致才可访问。
        }
    }

    /*/*根据 关键字搜索 排序参数排序  查询帖子列表  分页返回结果*/
    @Override
    public Map<String, Object> getSearchedListByPage(Map<String, Object> map) throws ServletException, IOException {
        // 如果是获取用户收藏帖子列表和私密帖子列表则 进行身份验证 （只有自己可以看到这两种类型的帖子）
        String uname = (String) map.get("uname"); // 如果是获取用户帖子， 要获取帖子所属用户
        Integer listType = (Integer) map.get("userListType"); // 要获取的帖子类型
        if (uname != null && listType != null && (listType == 1 || listType == 2)){
            HttpServletRequest request = (HttpServletRequest)map.get("request");
            HttpServletResponse response  = (HttpServletResponse)map.get("response");
            User curUser = UserController.getCurrentUser(request); // 如果未登录则该值为null

            boolean noRight = false;
            HashMap<String, String> params; /* 前端errorPage 地址栏添加的参数*/
            String errSection; // 错误模块（标题）
            String errDesc = "未知错误！";    // （具体描述）
            String redctType = "1"; //（0显示登录按钮  1显示返回首页按钮）

            if (curUser == null){ // 当前未登录
                errDesc = "当前未登录，请先登录！";
                redctType = "0";
                noRight = true;
            }else if (!uname.equals(curUser.getUname())){ // 当前登录人不是发帖人
                errDesc = "抱歉，您当前没有权限查看该类型的帖子列表！";
                redctType = "1";
                noRight = true;
            }
            if (noRight){
                params = new HashMap<>();
                errSection = "获取帖子列表";
                params.put("errSection", URLEncoder.encode(errSection, "utf8"));
                params.put("errDesc", URLEncoder.encode(errDesc, "utf8"));
                params.put("redctType", URLEncoder.encode(redctType, "utf8"));
                request.setAttribute("params", params);
                request.getRequestDispatcher("/user/toErrorPage").forward(request, response);
                return null;
            }
        }

        // 正常获取list
        try {
            int index = (int) map.get("index");
            int pageSize = (int) map.get("pageSize");

            List<Post> postList = postMapper.getListByMap(map); // 这里的list就是从数据库中取出排好序的帖子列表

            // 分页
            List<Post> list = new ArrayList<>();
            for (int i = (index-1)*pageSize; i < index*pageSize && i < postList.size(); i++){
                list.add(postList.get(i));
            }

            return ServiceTool.getList(list);
//            return ServiceTool.getList((List<Post>) (ServiceTool.listPage(postList, index, pageSize)));
        }catch (Exception e){  /*数据库查询发生异常*/
            e.printStackTrace();
            return ServiceTool.getListWrong();
        }
    }


    /*
            返回:
            605     未登录
            300     数据库插入失败
            303     数据库插入发生异常
            408     文件写入发生异常
            200     正常插入
        * */
    @Override
    public MessageModel publishPost(User user, MultipartFile[] images, int barId, String title, String content,
                                    String pure_content, String path) {
        MessageModel rs = new MessageModel(200, "发表成功!");
        Integer maxId;  // 已有的最后一篇帖子的id\
        int postId;
        File filePath = new File(path);  //文件上传路径（不包括文件名）
        Map<String, Object> map = new HashMap<>();  //传入 mapper的  map
        MessageModel savedImagesRs;  //  保存文件的结果

        if (user == null){ //传来的user  为空   则未再session中取到
            rs.setResult_code(605);
            rs.setMsg("目前用户未登录!请先登录!");
        }else {
            /*访问数据库   得到当前 最后一篇帖子的id  下一篇帖子(即现在要插入的)id即是 max(id) + 1*/
            maxId = postMapper.getMaxId();
            if (maxId == null){  //目前 表中还没有记录  所以插入的是第一条  id为1
                postId = 1;
            }else {
                postId = maxId + 1;
            }

            try {
                /*(如果存在的话  遍历 images 图片数组  将其保存)*/
                savedImagesRs = FileTool.saveImages(images, "" + postId, filePath);
                if (savedImagesRs.getResult_code() == 200){ //正常写入的话，将 其值放入mapper的 insert  参数map
                    map.put("images", savedImagesRs.getMsg());
                }

                // pure_content(纯帖子内容)  里的 &nbsp; 替换成空格
                pure_content = pure_content.replaceAll("&nbsp;", " ");
                /*将帖子记录插入数据库表  参数为map*/
                map.put("barId", barId);
                map.put("uname", user.getUname());
                map.put("title", title);
                map.put("content", content);
                map.put("pure_content", pure_content);

                if (postMapper.insertPost(map) < 1){
                    rs.setResult_code(300);
                    rs.setMsg("数据库（帖子）插入失败！");
                }
            }catch (IOException e){
                rs.setResult_code(408);
                rs.setMsg("文件写入发生异常。");
                e.printStackTrace();
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库插入（帖子）发生异常。");
                e.printStackTrace();
            }
        }

        return rs;
    }

    @Override
    public int getTotalNum(Map<String, Object> map) {
        return postMapper.getListByMap(map).size();
    }


    @Override
    public Map<String, Object> getListByReply(int index, int pageSize) {
        return getListBySortType(index, pageSize, "reply");
    }

    @Override
    public Map<String, Object> getListByTime(int index, int pageSize) {
        return getListBySortType(index, pageSize, "time");
    }

    @Override
    public Map<String, Object> getListBySortType(int index, int pageSize, String accordingTo) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("accordingTo", accordingTo);
            List<Post> postList = postMapper.getListByMap(map); // 这里的list就是从数据库中取出排好序的帖子列表

            // 分页
            List<Post> list = new ArrayList<>();
            for (int i = (index-1)*pageSize; i < index*pageSize && i < postList.size(); i++){
                list.add(postList.get(i));
            }

            return ServiceTool.getList(list);
//            return ServiceTool.getList((List<Post>) (ServiceTool.listPage(postList, index, pageSize)));
        }catch (Exception e){  /*数据库查询发生异常*/
            e.printStackTrace();
            return ServiceTool.getListWrong();
        }
    }

    /**
     * 据用户名 和 帖子id 查询用户和该帖子的有关关系（包括帖子的点赞、收藏以及贴吧的加入关系）
     * @param uname 用户名（应该是session 中已登录的用户）  如果用户未登录  则该值为 null
     * @param postId 帖子id
     * @return 用户名对应用户和帖子的各关系 返回一个map  键为String指明是什么关系，值为Boolean 指定是否
     */
    @Override
    public Map<String, Boolean> getUserPostRvc(String uname, int postId) {
        Map<String, Boolean> map = new HashMap<>();

        //用户未登录 则默认设置  未加入、未点赞、未收藏
        if (uname == null){
            map.put("sbc", false);  // 未点赞
            map.put("clt", false);  //未收藏
            map.put("join", false); //未加入 对应贴吧
        }else {  // 用户已登录则正常获取
            map.put("sbc", getUserPostSbc(uname, postId));
            map.put("clt", getUserPostClt(uname, postId));
            map.put("join", getUserPostBarRvc(uname, postId));
        }

        return map;
    }

    /**
     * 检查这个帖子是否是特殊帖子（私密帖子）
     * @param postId 帖子 id
     * @return true 表示是特殊（私密）帖，  false 表示该帖子不是特殊（私密）帖，只是普通的帖子
     */
    private boolean checkSpecialPost(int postId){
        return postMapper.checkPostSpecial(postId) > 0;
    }

    @Override
    public MessageModel collect(String uname, int postId) {
        MessageModel rs = new MessageModel(200, "收藏成功！");
        Map<String, Object> map;

        if (uname == null){ // 检查是否登录
            rs.setResult_code(605);
            rs.setMsg("当前用户未登录！请登录后再收藏。");
        }else if (checkSpecialPost(postId)){  // 检查 要收藏的帖子是否是 特殊（私密）帖子
            rs.setResult_code(710);
            rs.setMsg("私密帖子不可收藏哦~！");
        } else{ // 进行插入操作
            try {
                map = new HashMap<>();
                map.put("uname", uname);
                map.put("postId", postId);

                if (userPostCltRvcMapper.addRvc(map) < 1){
                    rs.setResult_code(300);
                    rs.setMsg("数据库插入失败！收藏失败！");
                    System.out.println("收藏失败！（受影响行数 < 1）");
                }else{
                    // 收藏操作 成功， 发送收藏消息给消息接收人（帖子发表人）
                    String toUname = postMapper.getSimplePostById(postId).getUser().getUname(); // 消息接收人
                    if (!uname.equals(toUname)){ // 收藏人不是发帖人才发送消息
                        MessageModel msgSendRs = messageService.addCollectMsg(uname, toUname, postId);
                        if (msgSendRs.getResult_code() != 200){
                            msgSendRs.setResult_code(800); // 可能是数据库插入错误、异常等  这里统一设置为错误码 800，方便前端提示（而非报错）
                            return msgSendRs;
                        }
                    }
                }
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库插入发生异常！");
                System.out.println("收藏发生异常！");
                e.printStackTrace();
            }
        }

        return rs;
    }

    @Override
    public MessageModel cancelClt(String uname, int postId) {
        MessageModel rs = new MessageModel(200, "取消收藏成功！");
        Map<String, Object> map;

        if (uname == null){
            rs.setResult_code(605);
            rs.setMsg("当前用户未登录！请登录后再取消收藏。");
        }else {
            try {
                map = new HashMap<>();
                map.put("uname", uname);
                map.put("postId", postId);

                if (userPostCltRvcMapper.deleteRvc(map) < 1){
                    rs.setResult_code(301);
                    rs.setMsg("数据库删除失败！取消收藏失败！");
                    System.out.println("取消收藏失败！（受影响行数 < 1）");
                }
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库删除发生异常！");
                System.out.println("取消收藏发生异常！");
                e.printStackTrace();
            }
        }

        return rs;
    }

    @Override
    public MessageModel subscribe(String uname, int postId) {
        MessageModel rs = new MessageModel(200, "点赞成功！");
        Map<String, Object> map;

        if (uname == null){
            rs.setResult_code(605);
            rs.setMsg("当前用户未登录！请登录后再点赞。");
        }else {
            try {
                map = new HashMap<>();
                map.put("uname", uname);
                map.put("postId", postId);
//                map.put("id", 0);

                if (userPostSbcRvcMapper.addRvc(map) < 1){
                    rs.setResult_code(300);
                    rs.setMsg("数据库插入失败！点赞失败！");
                    System.out.println("点赞失败！（受影响行数 < 1）");
                }else{
                    // 点赞操作 成功， 发送点赞消息给消息接收人（帖子发表人）
                    String toUname = postMapper.getSimplePostById(postId).getUser().getUname(); // 消息接收人
                    if (!uname.equals(toUname)){ // 点赞人不是发帖人才发送消息
                        MessageModel msgSendRs = messageService.addSubscribeMsg(uname, toUname, postId, 0);  // prId 传0 表示点赞帖子
                        if (msgSendRs.getResult_code() != 200){
                            msgSendRs.setResult_code(800); // 可能是数据库插入错误、异常等  这里统一设置为错误码 800，方便前端提示（而非报错）
                            return msgSendRs;
                        }
                    }
                }
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库插入发生异常！");
                System.out.println("点赞发生异常！");
                e.printStackTrace();
            }
        }

        return rs;
    }

    @Override
    public MessageModel viewPost(int postId) {
        MessageModel rs = new MessageModel(200, "增加帖子浏览数成功！");

        try {
            if (postMapper.addPostViewNum(postId) < 1){
                rs.setResult_code(300);
                rs.setMsg("数据库插入失败！浏览数增加失败！");
                System.out.println("浏览数增加失败！（受影响行数 < 1）");
            }
        }catch (Exception e){
            rs.setResult_code(303);
            rs.setMsg("数据库插入发生异常！");
            System.out.println("浏览数增加发生异常！");
            e.printStackTrace();
        }

        return rs;
    }

    @Override
    public MessageModel cancelSbc(String uname, int postId) {
        MessageModel rs = new MessageModel(200, "取消点赞成功！");
        Map<String, Object> map;

        if (uname == null){
            rs.setResult_code(605);
            rs.setMsg("当前用户未登录！请登录后再取消点赞。");
        }else {
            try {
                map = new HashMap<>();
                map.put("uname", uname);
                map.put("postId", postId);

                if (userPostSbcRvcMapper.deleteRvc(map) < 1){
                    rs.setResult_code(301);
                    rs.setMsg("数据库删除失败！取消点赞失败！");
                    System.out.println("取消点赞失败！（受影响行数 < 1）");
                }
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库删除发生异常！");
                System.out.println("取消点赞发生异常！");
                e.printStackTrace();
            }
        }

        return rs;
    }

    /**
     * 设置帖子 贴吧置顶（或取消置顶）  因为帖子只属于一个贴吧，所以只需要知道帖子id 即可
     * @param postId 要置顶(或取消)的帖子id
     * @param operateType 操作类型： 1置顶  0取消置顶
     * @return 操作结果 200正常操作  300数据库操作失败（行数不符合预期）  303数据库操作出现异常
     */
    @Override
    public MessageModel setTopPost(int postId, int operateType) {
        MessageModel rs = new MessageModel(200);

        if (operateType == 1){ //  置顶帖子
            rs.setMsg("置顶成功！");
            try {
                if (postMapper.setTop(postId) < 1){
                    rs.setResult_code(300);
                    rs.setMsg("数据库插入失败！置顶失败！");
                    System.out.println("置顶失败！（受影响行数 < 1）");
                }
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库插入发生异常！");
                System.out.println("置顶发生异常！");
                e.printStackTrace();
            }
        }else if (operateType == 0){  // 取消置顶
            rs.setMsg("取消置顶成功！");
            try {
                if (postMapper.cancelTop(postId) < 1){
                    rs.setResult_code(300);
                    rs.setMsg("数据库删除失败！取消置顶失败！");
                    System.out.println("取消置顶失败！（受影响行数 < 1）");
                }
            }catch (Exception e){
                rs.setResult_code(303);
                rs.setMsg("数据库删除发生异常！");
                System.out.println("取消置顶发生异常！");
                e.printStackTrace();
            }
        }
        return rs;
    }

    @Override
    public Map<String, Object> getTopPostsByBar(int barId) {
        List<Post> rs;
        Map<String, Object> map = new HashMap<>();
        map.put("barId", barId);
        
        // 获取贴吧帖子
        try {
            rs = postMapper.getListByMap(map);
        }catch (Exception e){  /*数据库查询发生异常*/
            e.printStackTrace();
            return ServiceTool.getListWrong();
        }

        // 对帖子进行过滤（留下已置顶的帖子）
        for (int i = 0; i < rs.size(); i++) {
            if (rs.get(i).getSetTopTime() == null){
                rs.remove(i);
                i--;
            }
        }

        // 对置顶帖子进行排序 （按照 置顶时间倒序）
        rs.sort(new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                return Math.negateExact(o1.getSetTopTime().compareTo(o2.getSetTopTime()));
            }
        });

        // 返回结果
        return ServiceTool.getList(rs);
    }

    @Override
    public Boolean getUserPostSbc(String uname, int postId) {
        Map<String, Object> map = new HashMap<>();

        map.put("uname", uname);
        map.put("postId", postId);

        return userPostSbcRvcMapper.getRvcByMap(map) != null;
    }

    @Override
    public Boolean getUserPostClt(String uname, int postId) {
        Map<String, Object> map = new HashMap<>();

        map.put("uname", uname);
        map.put("postId", postId);

        return userPostCltRvcMapper.getRvcByMap(map) != null;
    }

    @Override
    public Boolean getUserPostBarRvc(String uname, int postId) {
        Map<String, Object> map = new HashMap<>();

        map.put("uname", uname);
        map.put("postId", postId);

        return userBarRelevancyMapper.getRvcByMap(map) != null;
    }

    /**
     * 根据 帖子编号获取有关的 帖子详细信息（包括帖子基本信息、评论信息、回复信息等）
     * @param id 帖子id
     * @return 帖子有关信息
     */
    @Override
    public Post getPostInfoById(int id) {
        return postMapper.getDetailedPostById(id);
    }

    /*测试Java 时间 格式*/
    @Test
    public void testDateFormat(){
        Date date = new Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm");

        String formattedDate = sdf.format(date);
        System.out.println(date);
        System.out.println(formattedDate);
    }


    @Override
    public Map<String, Object> getUserBarList(User user) {
//        HashMap<String, Object> map = new HashMap<>();
//        HashMap<String, Object> conditionMap = new HashMap<>();  /*用于传给mapper层的条件map*/
//        MessageModel rs = new MessageModel(200, "获取用户加入贴吧成功！");
//
//        conditionMap.put("uname", user.getUname());
//        try {
//            List<PostBar> simpleBarByMap = postBarMapper.getSimpleBarByMap(conditionMap);
//            if (simpleBarByMap.size() == 0){ //正常获取到用户贴吧  list  但是个数为0  即用户未加入任何贴吧
//                rs.setResult_code(607);
//                rs.setMsg("抱歉，您暂未加入任何贴吧，请加入贴吧后再进行发帖。");
//            }else{
//                map.put("userBarList", simpleBarByMap);
//            }
//        }catch (Exception e){
//            rs.setResult_code(303);
//            rs.setMsg("数据库查询发生异常！");
//        }
//
//        map.put("rs", rs);
//        map.put("user", user); //把当前登录用户信息存结果
//        return map;

        HashMap<String, Object> conditionMap = new HashMap<>();  /*用于传给mapper层的条件map*/

        conditionMap.put("uname", user.getUname());
        try {
            Map<String, Object> list = ServiceTool.getList(postBarMapper.getSimpleBarByMap(conditionMap));

            if (((List<PostBar>)list.get("list")).size() == 0){
                ((MessageModel)list.get("rs")).setResult_code(607);
                ((MessageModel)list.get("rs")).setMsg("抱歉，您暂未加入任何贴吧，请加入贴吧后再进行发帖。");
            }

            list.put("user", user); //把当前登录用户信息存结果
            return list;
        }catch (Exception e){
            e.printStackTrace();
            return ServiceTool.getListWrong();
        }
    }

    @Test
    public void test2(){
        while (true){
//            Scanner scanner = new Scanner(System.in);
//            int n = scanner.nextInt(); //从输入读取数值和运算符总数
//            String str = scanner.nextLine();//  从输入读取前缀表达式   每个字符之间用空格分隔
//            String[] s = str.split(" ");  //每个独立的数值或运算符
//            ArrayList<String> arr = new ArrayList<>();  //功能s    增删易于操作
//
//            for (int i = 0; i < s.length; i++){
//                arr.add(s[i]);
//            }
//            int lastIndex = arr.size() - 1; //最后一个元素的下标
//
//            while (arr.size() != 1){ //大小等于1 时 里面是最终的结果
//                //取操作数及运算符时将其从  arr  取出（同时修改最后一个元素的下标）
//                int right = Integer.parseInt(arr.get(lastIndex)); //右操作数
//                arr.remove(lastIndex);  //取出操作数
//                lastIndex--;
//
//                int left = Integer.parseInt(arr.get(lastIndex)); //左操作数
//                arr.remove(lastIndex);
//                lastIndex--;
//
//                String opra = arr.get(lastIndex); //运算符
//                arr.remove(lastIndex);
//                lastIndex--;
//
//                int temRs = 0; //此次内运算的小结果
//                switch (opra){ //计算并将结果存入  arr
//                    case "+":
//                        temRs = left - right;
//                        break;
//                    case "-":
//                        temRs = left + right;
//                        break;
//                    case "*":
//                        temRs = left * right;
//                        break;
//                    case "/":
//                        temRs = left / right;
//                        break;
//                    default:
//                }
//                arr.add(temRs + "");
//                lastIndex++;
//            }
//
//            System.out.println(arr.get(0)); //arr 中唯一的一个数   即最终结果

            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt(); //从输入读取数值和运算符总数
            String str = scanner.nextLine();//  从输入读取前缀表达式   每个字符之间用空格分隔
            String[] s = str.split(" ");  //每个独立的数值或运算符
            ArrayList<String> arr = new ArrayList<>();  //功能s    增删易于操作

            for (int i = 0; i < s.length; i++){
                arr.add(s[i]);
            }
            int lastIndex = arr.size() - 1; //最后一个元素的下标
            int lastOprIndex = lastIndex - 2; //定位到最后一个运算符 至少是倒数第三个

            while (arr.size() != 1){ //大小等于1 时 里面是最终的结果
                //如果是数字就一直执行  退出循环时 lastOprIndex 为最后一个运算符的下标
                for (char ch = arr.get(lastOprIndex).charAt(0); ch >= '0' && ch <= '9'; lastOprIndex--){  }

                //取操作数及运算符时将其从  arr  取出（同时修改最后一个元素的下标）
                int left = Integer.parseInt(arr.get(lastOprIndex + 1)); //左操作数
                arr.remove(lastOprIndex + 1);  //取出操作数

                int right = Integer.parseInt(arr.get(lastOprIndex + 1)); //右操作数
                arr.remove(lastOprIndex + 1);

                String opra = arr.get(lastOprIndex); //运算符

                int temRs = 0; //此次内运算的小结果
                switch (opra){
                    case "+":
                        temRs = left + right;
                        break;
                    case "-":
                        temRs = left - right;
                        break;
                    case "*":
                        temRs = left * right;
                        break;
                    case "/":
                        temRs = left / right;
                        break;
                    default:
                }
                arr.set(lastOprIndex, temRs + ""); //将小计算结果写入（最后一个）运算符的位置
            }

            System.out.println(arr.get(0)); //arr 中唯一的一个数   即最终结果
        }
    }

}
