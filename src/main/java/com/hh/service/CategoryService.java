package com.hh.service;

import com.hh.pojo.Category;
import com.hh.pojo.MessageModel;

import java.util.List;
import java.util.Map;

public interface CategoryService {
    MessageModel addCategory(Category category);

    MessageModel judgeIdUsed(int id);

    Map<String, Object> getCategoryList();

    MessageModel deleteCategoryById(int id);
}
