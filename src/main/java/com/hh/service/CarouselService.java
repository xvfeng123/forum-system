package com.hh.service;

import com.hh.pojo.CarouselPost;
import com.hh.pojo.Category;
import com.hh.pojo.MessageModel;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface CarouselService {
    /**
     * 新增一个 滚动帖
     * @param carouselPost 滚动帖信息  其中的 描述和绑定帖子id有效
     * @param img 滚动帖图片
     * @param imgSavePath  滚动帖图片保存路径(不包括文件名)
     * @return 新增结果 rs  MessageModel
     */
    MessageModel addCarousel(CarouselPost carouselPost, MultipartFile img, String imgSavePath);

    /*根据 map 条件获取 滚动帖列表  --*/
    /*返回值包括 rs MessageModel 和 列表 */
    Map<String, Object> getCarouselListByMap(Map<String, Object> map);

    /**
     * 修改滚动帖的 （在首页显示的）顺序（序号）
     * @param list 调整位置后的滚动帖数组（那些 序号和index 不一致的元素就是要调整的 滚动帖）
     * @return 修改结果 MessageModel
     */
    MessageModel updateCrsListOrders(CarouselPost[] list);

    /**
     * 根据滚动帖的id（封在参数对象中） 更新一个 滚动帖信息（showOrder、img、postId或者desc  更新后的信息封在对象参数里）
     * 如果修改设置图片的话，应该把原来的图片文件删除 （直接覆盖可能会有图片类型不一致导致覆盖不了的问题）
     * @param carouselPost 更新后的滚动帖设置（id还是原来的id  其他属性可能会变）
     * @param img 新设置的图片文件
     * @param path 图片文件保存的路径 （不包括文件名）
     * @return 更新结果 rs  MessageModel
     */
    MessageModel updateCrsPostById(CarouselPost carouselPost, MultipartFile img, String path);

    /**
     * 根据滚动帖查询滚动帖的 简单细信息（不包括帖子的详细信息，比如标题、发表人信息等）
     * @param id 滚动帖id
     * @return 查询结果 包含查询结果rs MessageModel 和 查询到的滚动帖对象
     */
    Map<String, Object> getSimpleCrsById(int id);

    /*根据id删除一个滚动帖   （包括数据库记录和图片文件）  path 为图片文件保存的路径（不包括 文件名）*/
    MessageModel deleteCrsPostById(int id, String path);
}
