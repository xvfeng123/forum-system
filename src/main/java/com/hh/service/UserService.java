package com.hh.service;

import com.hh.pojo.MessageModel;
import com.hh.pojo.User;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface UserService {
    MessageModel insertUser(String uname, String upwd, String nickName, MultipartFile headImg, String motto,
                            String email, String birthday, String[] area, String intro, String path);

    MessageModel judgeUserExist(String uname);

    Map<String, Object> login(String uname, String upwd);

    MessageModel updateUser(String uname, String nickName, MultipartFile headImg, String motto,
                            String birthday, String[] area, String intro, String originalHeadImg, String path,
                            HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;

    /*通过用户名获取user*/
    Map<String, Object> getUserByName(String uname);

    /*通过用户名获取 simple  user  (不包含密码等敏感数据)*/
    Map<String, Object> getSPUserByName(String uname);

    /*检查用户 是否已登录     是否具有管理员权限*/
    MessageModel checkUserRight(HttpServletRequest request);

    /*检查user 是否具有管理员权限*/
    Boolean isAdmin(User user);

    /*根据用户名获取用户的 帖子数、评论数以及加入贴吧数，返回结果除了前面的三个数量结果外，还有获取结果rs messageModel*/
    Map<String, Object> getUserCountInfo(String uname);
}
