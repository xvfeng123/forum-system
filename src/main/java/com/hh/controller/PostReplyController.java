package com.hh.controller;

import com.hh.component.FilePathBase;
import com.hh.pojo.MessageModel;
import com.hh.pojo.User;
import com.hh.service.PostReplyService;
import com.hh.service.ReplyReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/*帖子评论 控制器*/
@RestController
@RequestMapping("pr")
//@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)//解决跨域访问  不加的话访问不了
public class PostReplyController {
    @Autowired
    private PostReplyService prService;

    /**
     * 对某条帖子  新增一条评论
     * @param files  前端发送过来的 评论数组附带的图片 文件  （可多张）
     * @param content  评论内容
     * @param postId   所说帖子id
     * @param request request 请求  用于获取当前登录用户
     * @return   插入结果 MessageBox(详情见service层的注释)
     */
    @PostMapping("pbPostReply")
    public MessageModel publishPostReply(@Nullable @RequestParam("files") MultipartFile[] files,
                                         @RequestParam("content") String content,
                                         @RequestParam("postId") int postId, HttpServletRequest request){
        User user = UserController.getCurrentUser(request);  //获取当前登录用户
//        String path = request.getServletContext().getRealPath("/static/images/postReplyImage/"); //存放帖子评论图片的地址
        String path = FilePathBase.baseUrl + FilePathBase.postReplyImage;   //存放帖子评论图片的地址

        return prService.publishPostReply(user, files, content, postId, path, request);
    }

    /*获取当前登录用户的用户名， 若未登录 则返回null*/
    private String getCuserUname(HttpServletRequest request){
        User user = UserController.getCurrentUser(request);
        return (user == null) ? null:user.getUname();
    }

    /*点赞帖子评论*/
    @GetMapping("sbcPr")
    public MessageModel sbcPost(HttpServletRequest request, @RequestParam("prId") int prId){
        return prService.subscribePr(getCuserUname(request), prId);
    }
    /*取消点赞帖子评论*/
    @GetMapping("cancelSbcPr")
    public MessageModel cancelSbc(HttpServletRequest request, @RequestParam("prId") int prId){
        return prService.cancelSbcPr(getCuserUname(request), prId);
    }
}
