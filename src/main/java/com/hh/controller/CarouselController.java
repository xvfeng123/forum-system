package com.hh.controller;

import com.hh.component.FilePathBase;
import com.hh.pojo.CarouselPost;
import com.hh.pojo.MessageModel;
import com.hh.pojo.User;
import com.hh.service.CarouselService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

// 滚动帖设置 controller
@RestController
@RequestMapping("carousel")
public class CarouselController {
    @Resource
    private CarouselService carouselService;

    /*新增一个 滚动帖*/
    /*根据字段名和参数名进行匹配， 这里通过接收到的参数只会初始化 postId 和 describe， id、showOrder通过数据库触发器写入，*/
    /*images 字段需要在 service 层进行处理  headImg 必须传递文件*/
    @PutMapping("/admin/addCrs")
    public MessageModel addCarouselPost(CarouselPost carouselPost, @RequestParam("file") MultipartFile headImg){
        String path = FilePathBase.baseUrl + FilePathBase.carouselPostImage;  //存放滚动帖设置图片的地址

        return carouselService.addCarousel(carouselPost, headImg, path);
    }

    /**
     * 获取已有的 滚动帖列表（包括帖子标题、发表人等详细信息）（按照序号排序）
     * @return 获取结果 和 滚动帖列表
     */
    @GetMapping("/getDetailedList")
    public Map<String, Object> getPostBarList(){
        return carouselService.getCarouselListByMap(null);
    }

    @PatchMapping("/admin/updateOrder")
//    public MessageModel updateCarouselOrder(CarouselPost[] crsList){
    public MessageModel updateCarouselOrder(@RequestBody CarouselPost[] crsList){ // 必须加@RequestBody 注解。不然会报没有构造器
        return carouselService.updateCrsListOrders(crsList);
    }

    /**
     * 根据滚动帖查询滚动帖的 简单细信息（不包括帖子的详细信息，比如标题、发表人信息等）
     * @param id 滚动帖id
     * @return 查询结果 包含查询结果rs MessageModel 和 查询到的滚动帖对象
     */
    @GetMapping("getSimpleCrs")
    public Map<String, Object> getSimpleCarouselInfoById(@RequestParam("crsId") int id){
        return carouselService.getSimpleCrsById(id);
    }

    /*更新一个用户个人资料(暂时不包括邮箱和密码)*/
    /*originalHeadImg  原来的头像文件名  （虽然头像是以用户名命名的，但是不同类型的图片会导致问题  所以此处还是得获取一下）*/
    @RequestMapping("/admin/updateCrs")
    public MessageModel updateCarouselPostById(CarouselPost carouselPost, @Nullable @RequestParam("file") MultipartFile img){
        String path = FilePathBase.baseUrl + FilePathBase.carouselPostImage;  //存放滚动帖图片的地址

        return carouselService.updateCrsPostById(carouselPost, img, path);
    }

    /**
     * 删除一个滚动帖（包括数据库记录和文件）
     * @param id 滚动帖id
     * @return 删除结果 messageModel 200正常 409文件删除失败 301记录删除失败 303记录删除异常
     */
    @DeleteMapping("/admin/delCrsById")
    public MessageModel deleteCrsPostById(@RequestParam("crsId") int id){
        String path = FilePathBase.baseUrl + FilePathBase.carouselPostImage;

        return carouselService.deleteCrsPostById(id, path);
    }
}
