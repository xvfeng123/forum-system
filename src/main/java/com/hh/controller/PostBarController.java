package com.hh.controller;

import com.hh.component.FilePathBase;
import com.hh.pojo.MessageModel;
import com.hh.pojo.PostBar;
import com.hh.pojo.User;
import com.hh.service.PostBarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("postBar")
//@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)//解决跨域访问
public class PostBarController {
    @Autowired
    private PostBarService postBarService;

    /*（当前登录用户）退出贴吧  */
    /*参数为  要退出的贴吧的id*/
    /*返回MessageModel
        result_code
        605  未登录
        301  数据库删除失败
        303  数据库删除异常
        200  成功退出贴吧（数据库删除成功）
    * */
    @GetMapping("exitBar")
    public MessageModel exitPostBar(@RequestParam("id") int barId, HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("user");  //从session中获取当前登录用户  若为null则表示未登录

        if (user == null){
            return new MessageModel(605, "当前暂未登录，请登录后再退出贴吧！");
        }
        String uname = user.getUname();
        return postBarService.userExitBar(uname, barId);
    }

    /*（当前登录用户）加入贴吧  */
    /*参数为  要加入的贴吧的id*/
    /*返回MessageModel
        result_code
        605  未登录
        300  数据库插入失败
        303  数据库插入异常
        200  成功加入贴吧（数据库插入成功）
    * */
    @GetMapping("joinBar")
    public MessageModel joinPostBar(@RequestParam("id") int barId, HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("user");  //从session中获取当前登录用户  若为null则表示未登录

        if (user == null){
            return new MessageModel(605, "当前暂未登录，请登录后再加入贴吧！");
        }
        String uname = user.getUname();
        return postBarService.userJoinBar(uname, barId);
    }

    /**
     * 查询贴吧详细信息 -- 名称、描述、头像，还包括 分类信息、 当前登录用户的加入情况
     * @param request 用于获取用户登录信息
     * @param barId 要查询的贴吧id
     * @return 查询结果
     */
    @GetMapping("/getDetailedBar")
    public Map<String, Object> getDetailedBarInfoById(HttpServletRequest request, @RequestParam("barId") int barId){
        User user = (User) request.getSession().getAttribute("user"); //当前登录用户
        String uname = user == null ? null : user.getUname();  //当前用户用户名，如果未登录（session中未取到user）则初始化未空字符串“”

        return postBarService.getDetailedBarInfoById(barId, uname);
    }

    /*查询当前登录用户（session中） 和接收到的贴吧编号对应的贴吧是否有关联关系（是否已加入该贴吧）
    * 已加入返回 601
    * 未加入返回 602
    * （如果未登录  直接访问首页  这时session中还并未有对应user  此时默认返回（前端表现为未加入） 605  ）
    * */
    @GetMapping("checkUserBar")
    public MessageModel checkUserBarRelevance(@RequestParam("barId") int barId, HttpServletRequest request){
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user"); //从session中取出当前登录的用户

        if (user == null){ //session中没有user  用户未登录
            return new MessageModel(605, "用户当前未登录！");
        }
        return postBarService.checkUserBarRelevance(user.getUname(), barId);
//        return new MessageModel(601, "当前用户已加入当前贴吧！");
    }

    /**
     * 根据条件（参数可以选择性发送）获取贴吧列表
     * 获取 贴吧列表信息的同时 获取当前登录用户（session中的user）和各贴吧的加入关系
     * @param request req
     * @param getRecommendBars 获取推荐贴吧 1获取推荐  0正常获取所有贴吧（默认）
     * @param unameIndex 查询某用户加入的贴吧
     * @param searchWd 搜索贴吧（按名称或 贴吧id 搜索）
     * @param searchType 0贴吧名称（默认）  1贴吧编号
     * @param sortType postsNum 帖子数最多倒序  peopleNum 人数最多倒序  否则按贴吧id
     * @param categoryId 查询该分类下的贴吧
     * @return 符合条件的贴吧列表 以及 当前用户加入
     */
    @GetMapping("getRvcBarList")
    public Map<String, Object> getRvcBarList(HttpServletRequest request,
                                             @RequestParam(value = "getRec", required = false, defaultValue = "0") Integer getRecommendBars,
                                             @RequestParam(value = "uname", required = false) String unameIndex,
                                             @RequestParam(value = "searchWd", required = false) String searchWd,
                                             @RequestParam(value = "searchType", required = false, defaultValue = "0") Integer searchType,
                                             @RequestParam(value = "sortType", required = false, defaultValue = "postsNum") String sortType,
                                             @RequestParam(value = "ctgId", required = false) Integer categoryId){
        Map<String, Object> map = new HashMap<>(); // sql 参数
        if (unameIndex != null){
            map.put("uname", unameIndex);
        }
        if (searchWd != null){
            map.put("searchWd", searchWd);
            map.put("searchType", searchType);
        }
        if (categoryId != null){
            map.put("ctgId", categoryId);
        }
        if (getRecommendBars != 0){
            map.put("getRec", getRecommendBars);
        }
        map.put("accordingTo", sortType);  // 结果排序方式  默认按照帖子数倒序

        Map<String, Object> list = postBarService.getPostBarListByMap(map);  /*里面包含一个MessageBox 和 一个PostBar list(如果查询不出错的话)*/
        User user = (User) request.getSession().getAttribute("user"); //当前登录用户
        String uname;  //当前用户用户名，如果未登录（session中未取到user）则初始化未空字符串“”

        if (user == null){
            uname = "";
        }else {
            uname = user.getUname();
        }
        if (((MessageModel)list.get("rs")).getResult_code() == 200){ //如果成功获取到贴吧列表则 继续查询当前用户和各贴吧的加入关系
            Map<Integer, Boolean> rvcMap = postBarService.getUserBarRvc(uname, (List<PostBar>)list.get("list"));
            list.put("rvcMap", rvcMap);
        }

        return list;
    }

    // patch 这种请求方法  一般用于 更新数据
    /**
     * 设置推荐贴吧 （将参数数组内的贴吧设置为推荐 -- 1  其他设置为 不推荐 -- 0）
     * @param barIds 要设置为推荐的贴吧的id 构成的数组
     * @return MessageModel 200 正常  303 数据库更新失败
     */
    @PatchMapping("/admin/setRecommendBars")
    public MessageModel setRecommendBars(@RequestParam("barIds") int[] barIds){
        return postBarService.setRecommendBars(barIds);
    }

    @GetMapping("/admin/getList")
    public Map<String, Object> getPostBarList(){
        return postBarService.getPostBarList();
    }

    @GetMapping("/admin/del")
    public MessageModel deletePostBar(@RequestParam("id") int id, HttpServletRequest request){
        // 按照下面的地址，如果打包到服务器上，地址获取不到，会出错
//        String path = request.getServletContext().getRealPath("/static/images/barAvatar/"); //存放贴吧头像的地址
        String path = FilePathBase.baseUrl + FilePathBase.barAvatar; //存放贴吧头像的地址
        return postBarService.deletePostBarById(id, path);
    }

    @PostMapping("/admin/add")
    public MessageModel addPostBar(String name, int[] checkedCategories, @RequestParam("file") MultipartFile headImg, String describe,
                                   HttpServletRequest request){
        System.out.println("name:" + name);
        System.out.println("describe:" + describe);
        System.out.println("checkedCategories:" + Arrays.toString(checkedCategories));
        System.out.println("file:" + headImg);

//        String path = request.getServletContext().getRealPath("/static/images/barAvatar/"); //存放贴吧头像的地址
        String path = FilePathBase.baseUrl + FilePathBase.barAvatar;  //存放贴吧头像的地址
        System.out.println("path:" + path);

        return postBarService.addPostBar(name, checkedCategories, headImg, describe, path);
    }

    @RequestMapping("/getUserIp")
    public String getUserIp(HttpServletRequest request){
        System.out.println("进入控制器...");

        String remoteAddr = request.getRemoteAddr();
        String header = request.getHeader("x-forwarded-for");
        System.out.println("客户端ip是-：" + remoteAddr);
        System.out.println("客户端ip2是--：" + header);

        return remoteAddr;
    }
}
