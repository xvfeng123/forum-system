package com.hh.controller;

import com.hh.pojo.Category;
import com.hh.pojo.MessageModel;
import com.hh.service.CategoryService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.MessageDigest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("category")
//@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)//解决跨域访问
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /* 查询当前分类编号id是否已使用过 */
    @RequestMapping("idUsed")
    public MessageModel checkIfIdUsed(@RequestParam("id") int id){
        return categoryService.judgeIdUsed(id);
    }


    /*添加一个（贴吧）类别*/
    @RequestMapping("/admin/add")
    public MessageModel addCategory(Category category){  /*根据字段名进行匹配*/
        return categoryService.addCategory(category);
    }

    /*获取分类列表*/
    @RequestMapping("getList")
    public Map<String, Object> getCategoryList(){
        return categoryService.getCategoryList();
    }

    @RequestMapping("/admin/del")
    public MessageModel deleteCategory(@RequestParam("id") int id){
        return categoryService.deleteCategoryById(id);
    }


}
