package com.hh.controller;

import com.hh.pojo.MessageModel;
import com.hh.pojo.User;
import com.hh.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("msg")
public class MessageController {
    @Resource
    private MessageService messageService;

    /**
     * 获取用户的某种消息列表（全获取）
     *
     * @param request 用于获取当前登录用户
     * @param type    要获取的消息类型  1评论 2回复 3点赞 4收藏
     * @return map 包括获取结果rs 和 结果列表list
     */
    @GetMapping("getUserMsgsByType")
    public Map<String, Object> getUserMsgsByType(HttpServletRequest request, @RequestParam("msgType") int type) {
        User currentUser = UserController.getCurrentUser(request);

        return messageService.getUserMsgsByType(type, currentUser == null ? null : currentUser.getUname());
    }


    /**
     * 获取用户（当前登录用户）的各种消息的未读数量
     *
     * @param request 用于获取当前登录用户
     * @return map 包括获取结果rs 和 四种消息类型的未读数量
     */
    @GetMapping("getMsgUnreadNums")
    public Map<String, Object> getUserUnreadNumsByMsgtype(HttpServletRequest request) {
        User currentUser = UserController.getCurrentUser(request);

        return messageService.getUserMsgUnreadNums(currentUser == null ? null : currentUser.getUname());
    }

    /**
     * 将用户的对应类型的消息都设置为 已读。
     * 在调用当前方法时，在前端已经判断用户该类型下是有未读消息的，所以如果 数据库受影响行数 < 1 表示异常
     *
     * @param request 用于获取当前登录用户 （ 消息接收人， 也是操作的消息所属的用户）
     * @param type    消息类型
     * @return 设置结果 rs messageModel
     */
    @PatchMapping("setMsgsRead")
    public MessageModel setUserMsgsReadByType(HttpServletRequest request, @RequestParam("msgType") int type) {
        User currentUser = UserController.getCurrentUser(request);

        return messageService.setUserMsgsReadByType(currentUser == null ? null : currentUser.getUname(), type);
    }

    /**
     * 清空（当前登录）用户的 某种类型的消息
     * @param request 用于获取用户
     * @param type 消息类型
     * @return 清空结果， 如果受影响行数为0（<1） 返回状态码 301
     */
    @DeleteMapping("truncateUserMsg")
    public MessageModel truncateUserMsgByType(HttpServletRequest request, @RequestParam("msgType") int type){
        User currentUser = UserController.getCurrentUser(request);

        return messageService.truncateUserMsgByType(currentUser == null ? null : currentUser.getUname(), type);
    }

}
