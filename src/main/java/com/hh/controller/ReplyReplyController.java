package com.hh.controller;

import com.hh.component.FilePathBase;
import com.hh.pojo.MessageModel;
import com.hh.pojo.User;
import com.hh.service.ReplyReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/*帖子评论回复 控制器*/
@RestController
@RequestMapping("rr")
//@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)//解决跨域访问  不加的话访问不了
public class ReplyReplyController {
    @Autowired
    private ReplyReplyService rrService;

    /**
     * 新增一条评论回复
     * @param files  前端发送过来的 评论数组附带的图片 文件  （可多张）
     * @param replyId 所归属的帖子评论id
     * @param rrId 对应回复的id（如果该回复是针对评论回复的话），默认为0 即说明该回复是针对评论本身的
     * @param content  回复内容
     * @param postId   所说帖子id
     * @param request request 请求  用于获取当前登录用户
     * @return   插入结果(详情见service层的注释)（包含一个Message box 以及 刚插入的replyReply 如果正常的话）
     */
    @PostMapping("pbRR")
    public Map<String, Object> publishReplyReply(@Nullable @RequestParam("files") MultipartFile[] files, @RequestParam("replyId") int replyId,
                                                 @RequestParam("rrId") int rrId, @RequestParam("content") String content,
                                                 @RequestParam("postId") int postId, HttpServletRequest request){
        User user = UserController.getCurrentUser(request);  //获取当前登录用户
//        String path = request.getServletContext().getRealPath("/static/images/postReplyReplyImage/"); //存放帖子图片的地址
        String path = FilePathBase.baseUrl + FilePathBase.postReplyReplyImage; //存放帖子图片的地址

        return rrService.publishRR(user, files, replyId, rrId, content, postId, path);
    }
}
