package com.hh.config;

import com.hh.component.FilePathBase;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/*虚拟地址映射*/
//把对服务器中静态资源（如帖子图片）的访问映射到对本机某个地址进行访问
// 如 把对  http://localhost:8081/static/images/postImage/xxx.jpg 改成访问本机的
// E:\IdeaSpringProjects\forum_system\src\main\webapp\static\images\postImage\xxx.jpg

@Configuration   //SpirngBoot 配置注释
@EnableWebMvc
public class StaticResourceConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String baseUrl = "file:" + FilePathBase.baseUrl; //文件基路径   注意这个 file: 必不可少
//        System.out.println("baseUrl:" + baseUrl); // file:E:\IdeaSpringProjects\forum_system\src\main\webapp\

        //当地址栏包含 /   那么就默认访问 项目中设置的文件保存基路径 文件夹
        registry.addResourceHandler("/**").addResourceLocations(baseUrl);
    }


    /*
    假如  当前boot服务器  实际访问地址是  http://localhost:8081/static/images/postImage/21-1.png

    这个时候替换成虚拟路径，  实际访问到的就是：
        本机的  file:E:\IdeaSpringProjects\forum_system\src\main\webapp\static\images\postImage\21-1.png


               其实就是相当于  地址中 http://localhost:8081/的那一段（注意包括斜杠/）
                                换成了我们设定的地址映射 baseUrl( file:E:\IdeaSpringProjects\forum_system\src\main\webapp\  )
    * */

}
