//package com.hh.config;
//
//import org.apache.catalina.Context;
//import org.apache.catalina.connector.Connector;
//import org.apache.tomcat.util.descriptor.web.SecurityCollection;
//import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class HttpToHttpsConfig {
//
//    /**
//     * http重定向到https
//     * @return
//     */
//    @Bean
//    public TomcatServletWebServerFactory servletContainer() {
//        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
//            @Override
//            protected void postProcessContext(Context context) {
//                SecurityConstraint constraint = new SecurityConstraint();
//                constraint.setUserConstraint("CONFIDENTIAL");
//                SecurityCollection collection = new SecurityCollection();
//                collection.addPattern("/*");
//                constraint.addCollection(collection);
//                context.addConstraint(constraint);
//            }
//        };
//        tomcat.addAdditionalTomcatConnectors(httpConnector());
//        return tomcat;
//    }
//
//    //以编程的方式配置HTTP connector，然后重定向到HTTPS connector
//    @Bean
//    public Connector httpConnector() {
//        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
//        connector.setScheme("http");
//        //Connector监听的http的端口号（这个端口不能已经被占用）
//        connector.setPort(8080);
//        connector.setSecure(false);
//        //监听到http的端口号后转向到的https的端口号
//        connector.setRedirectPort(8082);
//        return connector;
//
//        /*
//        * http  80  设置为此端口默认不显示
//        * https  443  设置为此端口默认不显示
//        *
//        * 比如我这里项目的端口号是8082，安装了SSL，
//        * 这里我监听访问http  时的端口号 8080
//        * 设置重定向跳转的https的端口号  8082
//        *
//        * （本地运行）
//        * 此时我浏览器访问 http://localhost:8080/category/getList 可以被正常重定向到  https://localhost:8082/category/getList
//        * 但是这里我不能设置监听访问 http 的端口号为 8082，他会报错说 8082 正在被使用（这里正是我这个项目设置的端口号）
//        * 也就是说  你不能针对一个项目，针对他这个项目端口， 把通过http 的访问 重定向到  https的访问
//        *           因为 你既然要用到这个项目，项目设置的端口必定是重定向后（https）的端口（比如这里的8082）
//        *           也就说说，这个8082已经会被占用
//        *           你要从http转过来，肯定也是地址栏访问8082端口，不过这里是http
//        *           此时，你就又必须监听8082，可他已经在使用了。所以无法做到
//        *           也就是说，你不可能通过以上方法 来对 一个项目的 http 访问自动重定向到  https的访问
//        *
//        *           除非，你http访问的地址端口（比如说这里8080）  不是  项目的真正端口（比如说这里8082）
//        *           http -->  https  必定是不同端口的跳转
//        *
//        *           而 http 到 https， 都是默认端口，除了多了个s，其实端口也变了，只是他默认都不显示
//        *           所以如果你项目端口是443（https默认），本来访问就不用写端口号，https就行了
//        *           然后是可以做到 http --> https的自动重定向的，
//        *           因为你访问 http，也不用写 端口号（这个80，默认，正常不显示），
//        *           所以相对于重定向后的，你表面上是少了个s，其实端口也不对，只是都默认不显示而已
//        *
//        *           也就是说，除非你项目本来就定义在 https 443 上，那样可以做到 http--> https 的重定向
//        *           否则，你项目使用自己的端口号（比如我这里的8082） 是不可能做到 直接进行 http--> https 的重定向的
//        * */
//    }
//}
