package com.hh.config;

import com.hh.component.AdminInterceptor;
import com.hh.component.CookieInterceptor;
import com.hh.component.PrivateResourceInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration  /*以spring的bean形式创建代理对象   加载一个springbean*/   /*运行过程中被执行*/
public class MyMvcConfig implements WebMvcConfigurer {

    //关键，将拦截器作为bean写入配置中
    @Bean // PrivateResourceInterceptor beanClass  getPrivateResourceInterceptor beanId，在原来的类上加了 @Component，就会生成两个这种bean，class一样，id不一样
    public PrivateResourceInterceptor getPrivateResourceInterceptor(){
        return new PrivateResourceInterceptor();
    }

    // 多个自定义拦截器存在时，registry.addInterceptor添加拦截器的顺序就是拦截器执行的顺序
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //addInterceptor  添加拦截的类（拦截器类）
        //addPathPatterns   添加要拦截的请求
        //excludePathPatterns  不拦截的需求
//        registry.addInterceptor(new CookieInterceptor()).addPathPatterns("/index").excludePathPatterns();
        registry.addInterceptor(new CookieInterceptor()).addPathPatterns("/**").excludePathPatterns();
//        registry.addInterceptor(new AdminInterceptor()).addPathPatterns("/*/admin/*", "/*");
        registry.addInterceptor(new AdminInterceptor()).addPathPatterns("/*/admin/*");

        // 注意这里一定要是 调用方法获取 对象，而不是 直接new，否则 service还是会注入失败（null）
        registry.addInterceptor(getPrivateResourceInterceptor()).addPathPatterns("/static/images/postImage/*");

    }
}
